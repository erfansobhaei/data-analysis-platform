from sqlalchemy import create_engine, Table

from DB.Table import Base


class DbContext:
    def __init__(self):
        engine = create_engine("sqlite+pysqlite:///:memory:", echo=False, future=True)
        Base.metadata.create_all(engine)
        self.engine = engine
        self.user_table = Table("user_account", Base.metadata, autoload_with=engine)
