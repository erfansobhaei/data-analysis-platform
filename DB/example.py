from sqlalchemy import insert, update, select
from sqlalchemy.orm import Session


def example(db_context):
    with db_context.engine.begin() as conn:
        result = conn.execute(
            insert(db_context.user_table),
            [
                {"name": "sandy", "fullname": "Sandy Cheeks"},
                {"name": "patrick", "fullname": "Patrick Star"}
            ]
        )
        print(result.inserted_primary_key_rows)

    with db_context.engine.begin() as conn:
        result = conn.execute(
            update(db_context.user_table).
            values(fullname="Patrick McStar").
            where(db_context.user_table.c.name == 'patrick')
        )
        print(result.rowcount)

    with Session(db_context.engine) as session:
        result = session.execute(
            select(db_context.user_table)
        ).all()
        print(result)


