import os
import dash_bootstrap_components as dbc
import pandas as pd
from dash import dcc
from dash import html
from dash.dependencies import Input, Output, State
from sklearn.cluster import DBSCAN
from app import app

df = pd.DataFrame()

FILEPATH = "./.var/uploads/"
home_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-home-96.png", style={"width": "50%"})], href="/", className="home_button")])

help_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-question-mark-96.png", style={"width": "50%"})], className="help_button",
            id="help_button")])

help = dbc.Offcanvas(
    html.P([
        "Here you can find a tutorial on how to use this page",
        "Here is a useful",
        html.A("link", href="google.com")]
    ),
    id="dbscan_help",
    title="Tutorial",
    is_open=False,
    close_button=True,
    placement="end"
)

modal = html.Div(
    [
        dbc.Modal(
            [
                dbc.ModalHeader(html.H5("Operation Failed!", style={"font-size": "25px"})),
                html.Br(),
                dbc.ModalBody(
                    "This is probably due to mismatch data in your dataset. Please try to clean your dataset first."),
                html.Br(),
            ],
            id="dbscan-modal",
            is_open=False,
        ),
    ]
)


def layout(df1, upload_id):
    # Read data from csv file
    global df
    df = df1.copy()

    # Create a list of dictionaries of features
    features = [{"label": str(col), "value": str(col)} for col in df.select_dtypes(include="number")]

    layout = dbc.Container([
        help,
        home_button,
        help_button,
        html.Div(upload_id, id="upload_id", style={"display": "none"}),
        # Headline of page
        dbc.Card([
            dbc.Row(
                dbc.Col(html.H2("DBSCAN Clustering"), width={"size": "auto"}),
                justify="center",
            ),
            dbc.Row([
                dbc.Col([
                    # Dropdown for selecting features
                    dbc.Label("Features:"),
                    dcc.Dropdown(
                        id="feature_t_c",
                        options=features,
                        value=[str(col) for col in df.columns],
                        placeholder="Select a feature",
                        multi=True,
                        style={"color": "black"},
                    ),
                ],
                    width={"size": 7},
                ),
            ],
                justify="center",
            ),
            dbc.Row([
                dbc.Col([
                    # Dropdown for selecting number of clusters
                    dbc.Label("eps:"),
                    dbc.Input(
                        id="eps_input",
                        type="number",
                        placeholder="The maximum distance between two samples",
                        style={"border": "1px solid #ccc"}
                    ),
                ],
                    width={"size": 4},
                ),
                dbc.Col([
                    # Dropdown for selecting number of clusters
                    dbc.Label("Min Samples:"),
                    dbc.Input(
                        id="min_samples_input",
                        type="number",
                        placeholder="The minimum number of samples in a neighborhood",
                        style={"border": "1px solid #ccc"}
                    ),
                ],
                    width={"size": 4},
                ),
            ],
                justify="center",
            ),
            dbc.Row([
                dbc.Col([
                    html.Br(),
                    dbc.Button(
                        "Submit",
                        color="success",
                        className="mr-1",
                        n_clicks=0,
                        id="click_submit",
                    ),
                ],
                    width={"size": "auto"},
                ),
            ],
                justify="center",
            ),
        ],
            body=True,
            style={"background-color": "white", "border-color": "#A6AFFF"}
        ),
        modal
    ],
        fluid=True,
    )
    return layout


# --------------- Functions for Callback Section -------------

@app.callback(
    Output("dbscan-modal", "is_open"),
    Input("click_submit", "n_clicks"),
    State("feature_t_c", "value"),
    State("eps_input", "value"),
    State("min_samples_input", "value"),
    State("upload_id", "children"),
    State("modal", "is_open"),
    prevent_initial_call=True,
)
def fit_clustering(click, features, eps, min_samples, upload_id, is_open):
    try:
        filename = os.listdir(FILEPATH + upload_id)[0]
        df = pd.read_csv(FILEPATH + upload_id + '/' + filename)
        clustering = DBSCAN(eps=eps, min_samples=min_samples).fit(df[features])
        df["DBSCAN"] = clustering.labels_
        df.to_csv(FILEPATH + upload_id + "/" + filename, encoding="utf_8_sig", index=False)
    except Exception as e:
        print(e)
        return True
    return False
