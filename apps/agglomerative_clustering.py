import dash_bootstrap_components as dbc
import numpy as np
import pandas as pd
import plotly.express as px
from dash import dcc
from dash import html
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
from sklearn.cluster import AgglomerativeClustering
from sklearn.preprocessing import normalize

from app import app

df = pd.DataFrame()
home_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-home-96.png", style={"width": "50%"})], href="/", className="home_button")])

help_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-question-mark-96.png", style={"width": "50%"})], className="help_button",
            id="help_button")])

help = dbc.Offcanvas(
    html.P([
        "Here you can find a tutorial on how to use this page",
        "Here is a usefull ",
        html.A("link", href="google.com")]
    ),
    id="agg_help",
    title="Tutorial",
    is_open=False,
    close_button=True,
    placement="end"
)


def layout(df1):
    # Read data from csv file
    global df
    df = df1.copy()
    df_target = df.copy()

    # Create a list of dictionaries in form of {'label':'2021', 'value': 2021}
    groups = [{"label": str(col), "value": str(col)} for col in df.columns]

    # Create a list of dictionaries of features
    features = [
        {"label": str(col), "value": str(col)}
        for col in df.select_dtypes(include="number")
    ]

    layout = dbc.Container(
        [
            help,
            help_button,
            home_button,
            # Headline of page
            dbc.Card(
                [
                    dbc.Row(
                        dbc.Col(
                            html.H2("Agglomerative Hierarchichal Clustering"),
                            width={"size": "auto"},
                        ),
                        justify="center",
                    ),
                    dbc.Row(
                        [
                            dbc.Col(
                                [
                                    # Dropdown for selecting group
                                    dbc.Label("Group by:"),
                                    dcc.Dropdown(
                                        id="group_agglomerative_c",
                                        options=groups,
                                        placeholder="Select one feature",
                                        multi=False,
                                        style={"color": "black"},
                                    ),
                                ],
                                width={"size": 4},
                            ),
                            dbc.Col(
                                [
                                    # Dropdown for selecting group
                                    dbc.Label("Groups:"),
                                    dcc.Dropdown(
                                        id="selected_groups_agglomerative_c",
                                        placeholder="Select a group",
                                        multi=True,
                                        style={"color": "black"},
                                    ),
                                ],
                                width={"size": 4},
                            ),
                            dbc.Col(
                                [
                                    # Dropdown for selecting features
                                    dbc.Label("Features:"),
                                    dcc.Dropdown(
                                        id="feature_agglomerative_c",
                                        options=features,
                                        placeholder="Select a feature",
                                        multi=True,
                                        style={"color": "black"},
                                    ),
                                ],
                                width={"size": 5},
                            ),
                            dbc.Col(
                                [
                                    # Dropdown for selecting number of clusters
                                    dbc.Label("Clusters:"),
                                    dbc.Input(
                                        id="n_agglomerative_c",
                                        type="number",
                                        min=1,
                                        max=32,
                                        step=1,
                                        style={"border": "1px solid #ccc"}
                                    ),
                                ],
                                width={"size": 1},
                            ),
                            dbc.Col(
                                [
                                    html.Br(),
                                    dbc.Button(
                                        "Submit",
                                        color="success",
                                        className="mr-1",
                                        n_clicks=0,
                                        id="click_agglomerative_c",
                                    ),
                                ],
                                width={"size": "auto"},
                            ),
                        ],
                        justify="center",
                    ),
                ],
                body=True,
                style={"background-color": "white", "border-color": "#A6AFFF"}
            ),
            # Graph component for showing result in form of a scatter plot
            dbc.Card(
                [
                    dbc.Row(
                        dbc.Col(
                            dcc.Loading(
                                children=[dcc.Graph(id="graph_agglomerative_c")],
                                color="#BDD5EF",
                                type="cube",
                                fullscreen=False,
                            ),
                            width={"size": 11},
                        ),
                        justify="center",
                    ),
                    dbc.Row(
                        dbc.Col(
                            dbc.Button(
                                "Export as .csv File",
                                color="primary",
                                n_clicks=0,
                                id="export_agglomerative_c",
                                style={"margin-top": "1.5rem"},
                            ),
                            width={"size": "auto"},
                        ),
                        justify="center",
                    ),
                    dcc.Download(id="export-csv_agglomerative_c"),
                ],
                body=True,
                style={"margin-top": "1rem", "background-color": "white", "border-color": "#A6AFFF"},
            ),
        ],
        fluid=True,
    )
    return layout


# --------------- Functions for Callback Section -------------


@app.callback(
    Output("selected_groups_agglomerative_c", "options"),
    Input("group_agglomerative_c", "value"),
    prevent_initial_call=True,
)
def update_groups(group_by):
    if not group_by:
        raise PreventUpdate
    items = list(pd.Series(df[group_by]).unique())
    options = [{"label": str(item), "value": item} for item in items]
    return options


@app.callback(
    Output("graph_agglomerative_c", "figure"),
    Input("click_agglomerative_c", "n_clicks"),
    State("group_agglomerative_c", "value"),
    State("selected_groups_agglomerative_c", "value"),
    State("feature_agglomerative_c", "value"),
    State("n_agglomerative_c", "value"),
    prevent_initial_call=True,
)
def update_graph(clck, group_by, group, feature, n):
    global df_target

    # If only one group was selected, it would be converted to form of a list
    if not isinstance(group, list):
        group = [group]
    # If only one feature was selected, it would be converted to form of a list
    if not isinstance(feature, list):
        feature = [feature]
    # Rank will be added to the feature as a compulsory feature for clustering
    # feature.append('Rank')

    # Create a new data frame for further use
    dff = pd.DataFrame(columns=["Label", "Value", "Cluster", "Group"])
    print(feature)
    df_scaled = normalize(df[feature])
    df_normalized = pd.DataFrame(df, columns=feature)

    # Update df_target for further export
    df_target = df[df[group_by].isin(group)].copy()
    df_target["Cluster"] = 0

    clusters = None

    # Find graph points for each of the selected group(s)
    for selected_group in group:
        # Get normalized numerical feature as input
        X = df_normalized.loc[df[group_by] == selected_group].reindex(columns=feature)

        # Prevent error for first run
        if len(X) == 0:
            break

        # Apply clustering
        model = AgglomerativeClustering(
            n_clusters=n, affinity="euclidean", linkage="ward"
        )
        model.fit_predict(X)

        # Add label to the dataframe
        df_with_label = df_normalized.loc[df[group_by] == selected_group].reindex(
            columns=feature
        )
        df_with_label["Label"] = model.labels_

        # Calculate the mean of each cluster
        df_centroids = df_with_label.groupby("Label").mean()
        centroids = df_centroids.to_numpy()

        # Change format of data for ease of plotting
        for i in range(len(centroids)):
            for j in range(len(centroids[i])):
                dff = dff.append(
                    {
                        "Label": feature[j],
                        "Value": centroids[i][j],
                        "Cluster": int(i + 1),
                        "Group": selected_group,
                    },
                    ignore_index=True,
                )
        sorted_clusters = (
            dff.loc[dff["Group"] == selected_group].groupby("Cluster")["Value"].sum()
        )
        sorted_clusters.sort_values(ascending=False, inplace=True)

        sorted_index = sorted_clusters.index.tolist()
        reindex = {sorted_index[i]: (i + 1) for i in range(len(sorted_index))}
        dff.loc[dff["Group"] == selected_group, "Cluster"] = dff["Cluster"].map(
            lambda x: reindex[int(x)]
        )
        dff.sort_values(by="Cluster", ascending=True, inplace=True)

        reindex_number = {
            i: (j + 1) for i, j in zip(df_centroids.index, range(len(centroids)))
        }
        clusters = (
            pd.DataFrame(model.labels_).applymap(lambda x: reindex_number[x]).to_numpy()
        )

        df_target.loc[df_target[group_by] == selected_group, "Cluster"] = clusters
        df_target.loc[df_target[group_by] == selected_group, "Cluster"] = df_target.loc[
            df_target[group_by] == selected_group, "Cluster"
        ].map(lambda x: reindex[x])

    dff["Cluster"] = dff["Cluster"].map(str)

    # Creating new plot for returning to output graph
    fig = px.scatter(
        dff,
        title='Agglomerative Clustering',
        x="Cluster",
        y="Value",
        color="Label",
        symbol="Label",
        range_y=[0, np.max(dff["Value"])],
        animation_frame="Group",
        template="plotly",
        hover_data=dff.columns,
    )
    fig["layout"]["xaxis"]["autorange"] = "reversed"
    return fig


@app.callback(
    Output("export-csv_agglomerative_c", "data"),
    Input("export_agglomerative_c", "n_clicks"),
    prevent_initial_call=True,
)
def export_data(n_clicks):
    return dcc.send_data_frame(
        df_target.to_csv, "clustered_data.csv", index=False, encoding="utf-8-sig"
    )


@app.callback(
    Output("agg_help", "is_open"),
    Input("help_button", "n_clicks"),
    prevent_initial_call=True,
)
def open_help(clck):
    return True
