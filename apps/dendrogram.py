import pandas as pd
from dash import dcc
from dash import html
from sklearn.preprocessing import normalize
import scipy.cluster.hierarchy as sch
from dash.dependencies import Input, Output, State
import dash_bootstrap_components as dbc
from app import app
import plotly.figure_factory as ff

df = pd.DataFrame()


def layout(df1):
    # Set dataframes
    global df
    df = df1.copy()

    # Create a list of dictionaries of features
    features = [
        {"label": str(col), "value": str(col)}
        for col in df.select_dtypes(include="number")
    ]

    # Create a list of dictionaries of methods
    methods = [
        {"label": "Single", "value": "single"},
        {"label": "Complete", "value": "complete"},
        {"label": "Average", "value": "average"},
        {"label": "Weighted", "value": "weighted"},
        {"label": "Centroid", "value": "centroid"},
        {"label": "Median", "value": "median"},
        {"label": "Ward", "value": "ward"},
    ]

    layout = dbc.Container(
        [
            # Headline of page
            dbc.Card(
                [
                    dbc.Row(
                        dbc.Col(
                            html.H2("Dendrogram"),
                            width={"size": "auto"},
                        ),
                        justify="center",
                    ),
                    dbc.Row(
                        [
                            dbc.Col(
                                [
                                    # Dropdown for selecting features
                                    dbc.Label("Features:"),
                                    dcc.Dropdown(
                                        id="features_input",
                                        options=features,
                                        placeholder="Select dendrogram features",
                                        multi=True,
                                        style={"color": "black"},
                                    ),
                                ],
                                width={"size": 5},
                            ),
                            dbc.Col(
                                [
                                    # Dropdown for selecting linkage method
                                    dbc.Label("Linkage Method:"),
                                    dcc.Dropdown(
                                        id="method_input",
                                        options=methods,
                                        placeholder="Select one method",
                                        clearable=False,
                                        multi=False,
                                        style={"color": "black"},
                                    ),
                                ],
                                width={"size": 4},
                            ),
                            dbc.Col(
                                [
                                    html.Br(),
                                    dbc.Button(
                                        "Submit",
                                        color="success",
                                        className="mr-1",
                                        n_clicks=0,
                                        id="click_dendrogram_c",
                                    ),
                                ],
                                width={"size": "auto"},
                            ),
                        ],
                        justify="center",
                    ),
                ],
                body=True,
                color="secondary",
            ),
            # Graph component for showing result in form of a scatter plot
            dbc.Card(
                [
                    dbc.Row(
                        dbc.Col(
                            dcc.Loading(
                                children=[dcc.Graph(id="dendrogram_graph")],
                                color="#BDD5EF",
                                type="cube",
                                fullscreen=False,
                            ),
                            width={"size": 11},
                        ),
                        justify="center",
                    ),
                ],
                body=True,
                color="secondary",
                style={"margin-top": "1rem"},
            ),
        ],
        fluid=True,
    )
    return layout


# --------------- Functions for Callback Section -------------


@app.callback(
    Output("dendrogram_graph", "figure"),
    Input("click_dendrogram_c", "n_clicks"),
    State("method_input", "value"),
    State("features_input", "value"),
    prevent_initial_call=True,
)
def update_graph(clck, method, features):

    # If only one feature was selected, it would be converted to form of a list
    if not isinstance(features, list):
        features = [features]

    # Normalize data
    data_normalized = normalize(df[features])
    data_normalized = pd.DataFrame(data_normalized, columns=features)

    # Set linkage method (default single)
    linkage_func = sch.single
    if method == "complete":
        linkage_func = sch.complete
    elif method == "average":
        linkage_func = sch.average
    elif method == "weighted":
        linkage_func = sch.weighted
    elif method == "centroid":
        linkage_func = sch.centroid
    elif method == "median":
        linkage_func = sch.median
    elif method == "ward":
        linkage_func = sch.ward

    # Create a new data frame for further use
    fig = ff.create_dendrogram(
        data_normalized,
        linkagefun=linkage_func,
    )

    # Hide horizontal axis
    fig.update_xaxes(visible=False, showticklabels=False)

    return fig
