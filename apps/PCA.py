import os

import dash_bootstrap_components as dbc
import numpy as np
import pandas as pd
from dash import dcc
from dash import html
from dash.dependencies import Input, Output, State
from sklearn.decomposition import PCA

from app import app

HIDE = {
    "display": "none",
}
FILEPATH = "./.var/uploads/"
home_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-home-96.png", style={"width": "50%"})], href="/", className="home_button")])

help_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-question-mark-96.png", style={"width": "50%"})], className="help_button",
            id="help_button")])

help = dbc.Offcanvas(
    html.P([
        "Here you can find a tutorial on how to use this page",
        "Here is a usefull ",
        html.A("link", href="google.com")]
    ),
    id="pca_help",
    title="Tutorial",
    is_open=False,
    close_button=True,
    placement="end"
)
actions = ["Number of Features", "Variance Ratio"]
action_list = [{"label": str(act), "value": str(act)} for act in actions]


def layout(df1, upload_id):
    df = df1.copy()
    columns = [{"label": str(col), "value": str(col)} for col in df.select_dtypes(include="number")]

    layout = dbc.Container(
        [
            help,
            home_button,
            help_button,
            html.Div(upload_id, id="upload_id", style={"display": "none"}),
            dbc.Card([
                # Headline of page
                dbc.Row(
                    dbc.Col(html.H2("PCA"), width={"size": "auto"}),
                    justify="center",
                ),
                dbc.Row([
                    dbc.Col([
                        # Dropdown for selecting year
                        dbc.Label("Choose Your Columns:"),
                        dcc.Dropdown(
                            id="column",
                            options=columns,
                            value=[str(col) for col in df.select_dtypes(include="number")],
                            multi=True,
                            placeholder="Select a feature",
                            style={"color": "black"},
                        )
                    ], width={"size": 7}),
                ], justify="center"),
                dbc.Row([
                    dbc.Col([
                        dbc.Label("Method"),
                        dcc.Dropdown(
                            id="id_method_pca",
                            options=action_list,
                            placeholder="Select a method",
                            style={"color": "black"},
                            multi=False,
                        ),
                    ], width={"size": 4}),
                    dbc.Col([
                        dbc.Label(children="Number of Features/Variance Ratio", id="method_label"),
                        dcc.Input(
                            id="n_components",
                            placeholder="Please enter Number of Features or Variance Ratio",
                            type="number",
                            style={"cursor": "text"},
                            className="Select-control",
                        ),
                    ], width={"size": 4}),
                ], justify="center"),
                dbc.Row([
                    dbc.Col([
                        html.Br(),
                        dbc.Button(
                            "Apply Transform",
                            color="success",
                            className="mr-1",
                            n_clicks=0,
                            id="id_transform",
                        ),
                    ], width={"size": "auto"}),
                    dbc.Col([
                        html.Br(),
                        dbc.Button(
                            "Calculate info",
                            color="primary",
                            className="mr-1",
                            n_clicks=0,
                            id="id_fit",
                        ),
                    ], width={"size": "auto"})
                ], justify="center")
            ],
                body=True,
                style={"background-color": "white", "border-color": "#A6AFFF"}
            ),
            # Graph component for showing result in form of a scatter plot
            dbc.Card(
                children=[],
                id="pca_container",
                body=True,
                style={"margin-top": "1rem", "background-color": "white", "border-color": "#A6AFFF"},
            ),
        ],
        fluid=True,
    )
    return layout


@app.callback(
    Output("pca_container", "children"),
    Input("id_transform", "n_clicks"),
    Input("id_fit", "n_clicks"),
    State("column", "value"),
    State("n_components", "value"),
    State("upload_id", "children"),
    prevent_initial_call=True
)
def apply_transform(click_transform, click_fit, columns, n_components, upload_id):
    filename = os.listdir(FILEPATH + upload_id)[0]
    df = pd.read_csv(FILEPATH + upload_id + '/' + filename)
    data = df[columns]
    pca = PCA(n_components=n_components)
    if click_transform > 0:
        result = pca.fit_transform(data)
        df = df.drop(columns, axis=1)
        new_columns = [f"pca_{i}" for i in range(result.shape[1])]
        df[new_columns] = result
        df.to_csv(FILEPATH + upload_id + "/" + filename, encoding="utf_8_sig", index=False)
        return dbc.Label("Transform Applied successfully")
    else:
        pca.fit(data)
        return dbc.Col([
            dbc.Row(f"Number of old Columns: {pca.n_features_}", justify="center"),
            dbc.Row(f"Number of new Columns: {pca.n_components_}", justify="center"),
            dbc.Row(f"Number of new Samples: {pca.n_samples_}", justify="center"),
            dbc.Row(f"Retained Variance Ratio: {round(np.sum(pca.explained_variance_ratio_), 4)}", justify="center"),
        ])


@app.callback(
    Output("method_label", "children"),
    Input("id_method_pca", "value"),
    prevent_initial_call=True
)
def change_label(val):
    return val


@app.callback(
    Output("pca_help", "is_open"),
    Input("help_button", "n_clicks"),
    prevent_initial_call=True,
)
def open_help(clck):
    return True
