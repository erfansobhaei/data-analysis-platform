import os
import dash_bootstrap_components as dbc
import pandas as pd
from dash import dash_table
from dash import dcc
import dash_daq as daq
from dash import html
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
from sklearn.preprocessing import MinMaxScaler, StandardScaler, RobustScaler, PowerTransformer
from app import app
import plotly.express as px
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix, classification_report


FILEPATH = "./.var/uploads/"

HIDE = {
    "display": "none",
}
home_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-home-96.png", style={"width": "50%"})], href="/", className="home_button")])

modal_knn_success = html.Div(
    [
        dbc.Modal(
            [
                dbc.ModalHeader(html.H5("Success!", style={"font-size": "25px"})),
                html.Br(),
                dbc.ModalBody("Selected action has been applied to the selected columns"),
                html.Br(),
            ],
            id="modal_knn_success",
            is_open=False,
        ),
    ])

help_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-question-mark-96.png", style={"width": "50%"})], className="help_button", id="help_button")])

help = dbc.Offcanvas(
            html.P([
                "In statistics, the k-nearest neighbors algorithm is a non-parametric supervised learning method first developed by Evelyn Fix and Joseph Hodges in 1951, and later expanded by Thomas Cover. It is used for classification and regression. In both cases, the input consists of the k closest training examples in a data set.",
                html.Br(),
                "For more information about implmentation:  ",
                html.A("link", href="http://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html")]
            ),
            id="knn_help",
            title="Tutorial",
            is_open=False,
            close_button=True,
            placement="end"
        )


def layout(df1, upload_id):
    # Read data from csv file
    df = df1.copy()

    output_graph = dcc.Loading(
                                children=[dcc.Graph(id="graph_knn")],
                                color="#BDD5EF",
                                type="cube",
                                fullscreen=False,
                            )
                            
    # Create a list of dictionaries of features
    numerics = []
    for col in df.columns:
        if df[col].describe().size == 8:
            numerics.append(col)

    columns = [{"label": str(col), "value": str(col)} for col in numerics]

    layout = dbc.Container(
        [
            help,
            help_button,
            home_button,
            modal_knn_success,
            html.Div(upload_id, id="upload_id", style={"display": "none"}),
            dbc.Card(
                [
                    # Headline of page
                    dbc.Row(
                        dbc.Col(html.H2("KNN Classifier"), width={"size": "auto"}),
                        justify="center",
                    ),
                    dbc.Row(
                        [
                            dbc.Col(
                                [
                                    dbc.Label("X"),
                                    dcc.Dropdown(
                                        id="id_x_knn",
                                        options=columns,
                                        placeholder="Select a column",
                                        style={"color": "black"},
                                        multi=True
                                    ),
                                    
                                    dbc.Label("y"),
                                    dcc.Dropdown(
                                        id="id_y_knn",
                                        options=columns,
                                        placeholder="Select a column",
                                        style={"color": "black"},
                                        multi=False
                                    ),

                                ],
                                width={"size": 5},
                            ),
                            dbc.Col(
                                [
                                    # Dropdown for selecting year
                                    dbc.Label("Number of Neighbors"),
                                    dcc.Input(
                                        id="id_neighbors_knn",
                                        type="number",
                                        min=1,
                                    ),
                                ],
                                width={"size": 2},
                            ),
                        ],
                        justify="center",
                    ),
                    dbc.Row([
                            dbc.Col(
                                [
                                    # Dropdown for selecting number of clusters
                                    dbc.Label("Test split"),
                                    dbc.Input(
                                        id="test_split",
                                        type="number",
                                        min=0,
                                        max=99,
                                        step=1,
                                        value=10,
                                        style={"border": "1px solid #ccc"}
                                    ),
                                ],
                                width={"size": 1},
                            ),
                            dbc.Col(
                            daq.BooleanSwitch(
                                id="shuffle",
                                label="Shuffle",
                                labelPosition="top",
                                on=False,
                                color="green",
                                style={"margin-bottom": "15px"},
                            ),
                            width={"size": "auto"},
                        ),
                    ],
                        justify="center",
                        id="knn_adv",
                        style = HIDE
                    ),
                    dbc.Row([
                        dbc.Col(
                                [
                                    html.Br(),
                                    dbc.Button(
                                        "more options ...",
                                        color="info",
                                        className="mr-1",
                                        n_clicks=0,
                                        id="knn_adv_button",
                                    ),
                                ],
                                width={"size": "auto"},
                            ),
                        dbc.Col(
                            [
                                html.Br(),
                                dbc.Button(
                                    "Submit",
                                    color="success",
                                    className="mr-1",
                                    n_clicks=0,
                                    id="id_submit",
                                ),
                            ],
                            width={"size": "auto"},
                        ),
                    ],
                        justify="center",
                    )
                ],
                body=True,
                style={"background-color": "white", "border-color": "#A6AFFF"}
            ),
            # Graph component for showing result in form of a scatter plot
            dbc.Card([
                dbc.Row(
                    dbc.Col(
                        output_graph,
                        width={"size": 11},
                    ),
                    justify="center",
                ),

                ],
                body=True,
                style={"margin-top": "1rem", "background-color": "white", "border-color": "#A6AFFF"},
            ),
        ],
        fluid=True,
    )

    return layout


# --------------- Functions for Callback Section -------------

@app.callback(
    Output("graph_knn", "figure"),
    Input("id_submit", "n_clicks"),
    State("id_neighbors_knn", "value"),
    State("id_x_knn", "value"),
    State("id_y_knn", "value"),
    State("upload_id", "children"),
    prevent_initial_call=True,
)
def update_graph(click, neighbors, X, y, upload_id):
    # If only one group was selected, it would be converted to form of a list
    if not isinstance(X, list):
        X = [X]
    if len(X) == 0 or neighbors == None or len(y)==0:
        raise PreventUpdate
    filename = os.listdir(FILEPATH + upload_id)[0]
    df = pd.read_csv(FILEPATH + upload_id + '/' + filename)

    clf, y_pred = classify_knn(df, X, y, neighbors)

    report = classification_report(df[[y]], y_pred)


    fig = px.imshow(confusion_matrix(df[[y]], y_pred))
    return fig

@app.callback(
    Output("knn_help", "is_open"),
    Input("help_button", "n_clicks"),
    prevent_initial_call=True,
)
def open_help(clck):
    return True


@app.callback(
    Output("export_knn", "data"),
    Input("export_knn_btn", "n_clicks"),
    State("upload_id", "children"),
    prevent_initial_call=True,
)
def export_data(n_clicks, upload_id):
    filename = os.listdir(FILEPATH + upload_id)[0]
    df = pd.read_csv(FILEPATH + upload_id + '/' + filename)


    return dcc.send_data_frame(
        df.to_csv, "knn_result.csv", index=False, encoding="utf-8-sig"
    )


def classify_knn(df, X, y, n_neighbors):
    dfX = df[X]
    dfy = df[[y]]
    clf = KNeighborsClassifier(n_neighbors=n_neighbors)
    clf.fit(dfX, dfy)
    preds = clf.predict(dfX)
    return clf, preds

@app.callback(
    Output("knn_adv_button", "children"),
    Output("knn_adv", "style"),
    Input("knn_adv_button", "n_clicks"),
    State("knn_adv", "style"),
    prevent_initial_call=True,
)
def show_more(click, style):
    if style == HIDE:
        return "less options ...", {}
    else:
        return "more options ...", HIDE