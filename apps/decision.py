import os
import pickle
import plotly.graph_objects as go
import dash_bootstrap_components as dbc
import pandas as pd
from dash import dash_table
import dash_daq as daq
from dash import dcc
from dash import html
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
from app import app
import plotly.express as px
from sklearn.tree import DecisionTreeClassifier

FILEPATH = "./.var/uploads/"

HIDE = {
    "display": "none",
}
home_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-home-96.png", style={"width": "50%"})], href="/", className="home_button")])

modal_knn_success = html.Div(
    [
        dbc.Modal(
            [
                dbc.ModalHeader(html.H5("Success!", style={"font-size": "25px"})),
                html.Br(),
                dbc.ModalBody("Selected action has been applied to the selected columns"),
                html.Br(),
            ],
            id="modal_dtree_success",
            is_open=False,
        ),
    ])

help_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-question-mark-96.png", style={"width": "50%"})], className="help_button", id="help_button")])

help = dbc.Offcanvas(
            html.P([
                "A decision tree is a decision support tool that uses a tree-like model of decisions and their possible consequences, including chance event outcomes, resource costs, and utility. It is one way to display an algorithm that only contains conditional control statements.",
                html.Br(),
                "Decision trees are commonly used in operations research, specifically in decision analysis, to help identify a strategy most likely to reach a goal, but are also a popular tool in machine learning.",
                html.Br(),
                "For more information about implmentation:  ",
                html.A("link", href="https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html")]
            ),
            id="decision_help",
            title="Tutorial",
            is_open=False,
            close_button=True,
            placement="end"
        )


def layout(df1, upload_id):
    # Read data from csv file
    df = df1.copy()

    output_graph = dcc.Loading(
                                children=[dcc.Graph(id="graph_dtree")],
                                color="#BDD5EF",
                                type="cube",
                                fullscreen=False,
                            )

    # Create a list of dictionaries of features
    numerics = []
    for col in df.columns:
        if df[col].describe().size == 8:
            numerics.append(col)

    columns = [{"label": str(col), "value": str(col)} for col in numerics]

    layout = dbc.Container(
        [
            help,
            help_button,
            home_button,
            modal_knn_success,
            html.Div(upload_id, id="upload_id", style={"display": "none"}),
            dbc.Card(
                [
                    # Headline of page
                    dbc.Row(
                        dbc.Col(html.H2("Decision Tree Classifier"), width={"size": "auto"}),
                        justify="center",
                    ),
                    dbc.Row(
                        [
                            dbc.Col(
                                [
                                    dbc.Label("Features"),
                                    dcc.Dropdown(
                                        id="id_x_dtree",
                                        options=columns,
                                        placeholder="Select a column",
                                        style={"color": "black"},
                                        multi=True
                                    ),], width={"size": 5}),
                            dbc.Col([      
                                    dbc.Label("Label"),
                                    dcc.Dropdown(
                                        id="id_y_dtree",
                                        options=columns,
                                        placeholder="Select a column",
                                        style={"color": "black"},
                                        multi=False
                                    ),
                                ],
                                width={"size": 5},
                            ),
                        ],
                        justify="center",
                    ),
                    dbc.Row([
                            dbc.Col(
                                [
                                    # Dropdown for selecting number of clusters
                                    dbc.Label("Test split"),
                                    dbc.Input(
                                        id="test_split",
                                        type="number",
                                        min=0,
                                        max=99,
                                        step=1,
                                        value=10,
                                        style={"border": "1px solid #ccc"}
                                    ),
                                ],
                                width={"size": 1},
                            ),
                            dbc.Col(
                            daq.BooleanSwitch(
                                id="shuffle",
                                label="Shuffle",
                                labelPosition="top",
                                on=False,
                                color="green",
                                style={"margin-bottom": "15px"},
                            ),
                            width={"size": "auto"},
                        ),
                    ],
                        justify="center",
                        id="decision_adv",
                        style = HIDE
                    ),
                    dbc.Row([
                        dbc.Col(
                                [
                                    html.Br(),
                                    dbc.Button(
                                        "more options ...",
                                        color="info",
                                        className="mr-1",
                                        n_clicks=0,
                                        id="decision_adv_button",
                                    ),
                                ],
                                width={"size": "auto"},
                            ),
                        dbc.Col(
                            [
                                html.Br(),
                                dbc.Button(
                                    "Submit",
                                    color="success",
                                    className="mr-1",
                                    n_clicks=0,
                                    id="id_submit",
                                ),
                            ],
                            width={"size": "auto"},
                        ),
                    ],
                        justify="center",
                    )
                ],
                body=True,
                style={"background-color": "white", "border-color": "#A6AFFF"}
            ),
            # Graph component for showing result in form of a scatter plot
            dbc.Card([
                dbc.Row(
                    dbc.Col(
                        output_graph,
                        width={"size": 11},
                    ),
                    justify="center",
                ),
                ],
                body=True,
                style={"margin-top": "1rem", "background-color": "white", "border-color": "#A6AFFF"},
            ),
        ],
        fluid=True,
    )

    return layout


# --------------- Functions for Callback Section -------------

@app.callback(
    Output("graph_dtree", "figure"),
    Output("modal_dtree_success", "is_open"),
    Input("id_submit", "n_clicks"),
    State("id_x_dtree", "value"),
    State("id_y_dtree", "value"),
    State("upload_id", "children"),
    prevent_initial_call=True,
)
def update_graph(click, X, y, upload_id):
    # If only one group was selected, it would be converted to form of a list
    if not isinstance(X, list):
        X = [X]
    if len(X) == 0 or len(y) == 0:
        raise PreventUpdate
    filename = os.listdir(FILEPATH + upload_id)[0]
    df = pd.read_csv(FILEPATH + upload_id + '/' + filename)

    clf = classify_dtree(df, X, y)

    labels = [''] * clf.tree_.node_count
    parents = [''] * clf.tree_.node_count
    labels[0] = 'root'
    for i, (f, t, l, r) in enumerate(zip(
        clf.tree_.feature,
        clf.tree_.threshold,
        clf.tree_.children_left,
        clf.tree_.children_right,
    )):
        if l != r:
            labels[l] = f'{X[f]} <= {t:g}'
            labels[r] = f'{X[f]} > {t:g}'
            parents[l] = parents[r] = labels[i]
    fig = go.Figure(go.Treemap(
        branchvalues='total',
        labels=labels,
        parents=parents,
        values=clf.tree_.n_node_samples,
        textinfo='label+value+percent root',
        marker=dict(colors=clf.tree_.impurity),
        customdata=list(map(str, clf.tree_.value)),
        hovertemplate='''
    <b>%{label}</b><br>
    impurity: %{color}<br>
    samples: %{value} (%{percentRoot:%.2f})<br>
    value: %{customdata}'''
    ))
    return fig, True

@app.callback(
    Output("decision_help", "is_open"),
    Input("help_button", "n_clicks"),
    prevent_initial_call=True,
)
def open_help(clck):
    return True



def classify_dtree(df, X, y):
    clf = DecisionTreeClassifier()
    dfX = df[X]
    dfy = df[[y]]
    clf.fit(dfX, dfy)
    return clf

@app.callback(
    Output("decision_adv_button", "children"),
    Output("decision_adv", "style"),
    Input("decision_adv_button", "n_clicks"),
    State("decision_adv", "style"),
    prevent_initial_call=True,
)
def show_more(click, style):
    if style == HIDE:
        return "less options ...", {}
    else:
        return "more options ...", HIDE