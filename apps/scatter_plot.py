from pandas.core.algorithms import isin, mode
import pandas as pd
import numpy as np
import plotly.express as px
from dash import dcc
from dash import html
import dash_daq as daq
from dash.exceptions import PreventUpdate
from dash.dependencies import Input, Output, State
from sklearn.cluster import KMeans
import dash_bootstrap_components as dbc
from app import app

df = pd.DataFrame()
HIDE = {
    "display": "none",
}
home_button = html.Div([html.A([html.Img(src="../assets/icons8-home-96.png", style={"width":"50%"})],href="/", className="home_button")])
help_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-question-mark-96.png", style={"width": "50%"})], className="help_button", id="help_button")])

help = dbc.Offcanvas(
            html.P([
                "Here you can find a tutorial on how to use this page",
                "Here is a usefull ",
                html.A("link", href="google.com")]
            ),
            id="scatter_help",
            title="Tutorial",
            is_open=False,
            close_button=True,
            placement="end"
        )
def layout(df1):
    # Read data from csv file
    global df
    df = df1.copy()
    df_target = df.copy()

    # Create a list of dictionaries of features
    columns = [{"label": str(col), "value": str(col)} for col in df.columns]

    layout = dbc.Container(
        [
            help,
            help_button,
            home_button,
            dbc.Card(
                [
                    # Headline of page
                    dbc.Row(
                        dbc.Col(html.H2("Scatter Plot"), width={"size": "auto"}),
                        justify="center",
                    ),
                    dbc.Row(
                        [
                            dbc.Col(
                                [
                                    # Dropdown for selecting year
                                    dbc.Label("Independent Feature (X):"),
                                    dcc.Dropdown(
                                        id="id_independent_feature",
                                        options=columns,
                                        placeholder="Select a feature",
                                        style={"color": "black"},
                                    ),
                                ],
                                width={"size": 5},
                            ),
                            dbc.Col(
                                [
                                    # Dropdown for selecting year
                                    dbc.Label("Dependent Feature (Y):"),
                                    dcc.Dropdown(
                                        id="id_dependent_feature",
                                        options=columns,
                                        placeholder="Select a feature",
                                        style={"color": "black"},
                                    ),
                                ],
                                width={"size": 5},
                            ),
                            
                        ],
                        justify="center",
                    ),
                    dbc.Row([
                        dbc.Col(
                                [
                                    # Dropdown for selecting features
                                    dbc.Label("Group by:"),
                                    dcc.Dropdown(
                                        id="id_group_by",
                                        options=columns,
                                        placeholder="Select a feature",
                                        style={"color": "black"},
                                    ),
                                ],
                                width={"size": 5},
                            ),
                            dbc.Col(
                                [
                                    # Dropdown for selecting features
                                    dbc.Label("Groups:"),
                                    dcc.Dropdown(
                                        id="scatter_plot_id_groups",
                                        placeholder="Select group(s) to show",
                                        multi=True,
                                        style={"color": "black"},
                                    ),
                                ],
                                width={"size": 5},
                            ),
                    ],
                        justify="center",
                        id="scatter_plot_adv",
                        style = HIDE
                    ),
                    dbc.Row([
                        dbc.Col(
                                [
                                    html.Br(),
                                    dbc.Button(
                                        "Submit",
                                        color="success",
                                        className="mr-1",
                                        n_clicks=0,
                                        id="id_submit",
                                    ),
                                ],
                                width={"size": "auto"},
                            ),
                        dbc.Col(
                                [
                                    html.Br(),
                                    dbc.Button(
                                        "more options ...",
                                        color="info",
                                        className="mr-1",
                                        n_clicks=0,
                                        id="scatter_plot_adv_button",
                                    ),
                                ],
                                width={"size": "auto"},
                            )
                            ],
                        justify="center",
                    )
                ],
                body=True,
                style={"background-color":"white", "border-color":"#A6AFFF"}
            ),
            # Graph component for showing result in form of a scatter plot
            dbc.Card(
                dbc.Row(
                    dbc.Col(
                        dcc.Loading(
                            children=[dcc.Graph(id="scatter_plot_graph")],
                            color="#BDD5EF",
                            type="cube",
                            fullscreen=False,
                        ),
                        width={"size": 11},
                    ),
                    justify="center",
                ),
                body=True,
                style={"margin-top": "1rem", "background-color":"white", "border-color":"#A6AFFF"},
            ),
        ],
        fluid=True,
    )

    return layout


# --------------- Functions for Callback Section -------------


@app.callback(
    Output("scatter_plot_id_groups", "options"),
    Input("id_group_by", "value"),
    prevent_initial_call=True,
)
def update_groups(group_by):
    if not group_by:
        raise PreventUpdate
    items = list(pd.Series(df[group_by]).unique())
    options = [{"label": str(item), "value": item} for item in items]
    return options


@app.callback(
    Output("scatter_plot_graph", "figure"),
    Input("id_submit", "n_clicks"),
    State("id_group_by", "value"),
    State("scatter_plot_id_groups", "value"),
    State("id_independent_feature", "value"),
    State("id_dependent_feature", "value"),
    prevent_initial_call=True,
)
def update_graph(click, group_by, groups, independent, dependent):
    # If only one group was selected, it would be converted to form of a list
    if not isinstance(groups, list):
        groups = [groups]

    dff = df.copy()

    if group_by and groups:
        dff = dff[dff[group_by].isin(groups)]
        dff[group_by] = dff[group_by].apply(str)

    # Creating new plot for returning to output graph
    fig = px.scatter(
        dff,
        x=independent,
        y=dependent,
        color=group_by,
        symbol=group_by,
        template="plotly",
        hover_data=dff.columns,
    )
    return fig

@app.callback(
    Output("scatter_plot_adv_button", "children"),
    Output("scatter_plot_adv", "style"),
    Input("scatter_plot_adv_button", "n_clicks"),
    State("scatter_plot_adv", "style"),
    prevent_initial_call=True,
)
def show_more(click, style):
    if style == HIDE:
        return "less options ...", {}
    else:
        return "more options ...", HIDE

@app.callback(
    Output("scatter_help", "is_open"),
    Input("help_button", "n_clicks"),
    prevent_initial_call=True,
)
def open_help(clck):
    return True