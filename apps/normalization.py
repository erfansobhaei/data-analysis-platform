import os
import dash_bootstrap_components as dbc
import pandas as pd
from dash import dash_table
from dash import dcc
from dash import html
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
from sklearn.preprocessing import MinMaxScaler, StandardScaler, RobustScaler, PowerTransformer
from app import app

FILEPATH = "./.var/uploads/"

HIDE = {
    "display": "none",
}
home_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-home-96.png", style={"width": "50%"})], href="/", className="home_button")])

modal_norm_success = html.Div(
    [
        dbc.Modal(
            [
                dbc.ModalHeader(html.H5("Success!", style={"font-size": "25px"})),
                html.Br(),
                dbc.ModalBody("Selected action has been applied to the selected columns"),
                html.Br(),
            ],
            id="modal_norm_success",
            is_open=False,
        ),
    ])

help_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-question-mark-96.png", style={"width": "50%"})], className="help_button", id="help_button")])

help = dbc.Offcanvas(
            html.P([
                "Here you can find a tutorial on how to use this page",
                "Here is a usefull ",
                html.A("link", href="google.com")]
            ),
            id="normalization_help",
            title="Tutorial",
            is_open=False,
            close_button=True,
            placement="end"
        )

def info_provider(df):
    tdata = []
    for column in df.columns:
        info = {}
        if df[column].describe().size == 8:
            info["Column"] = column
            info["Count"] = df[column].describe()["count"]
            info["Missing"] = df[column].isnull().sum()
            info["Mean"] = df[column].describe()["mean"]
            info["STD"] = df[column].describe()["std"]
            info["Min"] = df[column].describe()["min"]
            info["Max"] = df[column].describe()["max"]
            info["Unique"] = "-"
        else:
            info["Column"] = column
            info["Count"] = df[column].describe()["count"]
            info["Missing"] = df[column].isnull().sum()
            info["Mean"] = "-"
            info["STD"] = "-"
            info["Min"] = "-"
            info["Max"] = "-"
            info["Unique"] = df[column].describe()["unique"]
        tdata.append(info)
    return tdata


def layout(df1, upload_id):
    # Read data from csv file
    df = df1.copy()
    tdata = info_provider(df)

    info_table = dash_table.DataTable(
        id="info_table_norm",
        columns=[
            {
                "name": i,
                "id": i,
            }
            for i in ["Column", "Count", "Missing", "Mean", "STD", "Min", "Max", "Unique"]
        ],
        data=tdata,
        sort_action="native",
        sort_mode="single",
        page_action="native",
        page_current=0,
        style_table={"overflowX": "auto"},
        page_size=20,
        style_header={
            "color": "black",
        },
        style_cell={
            "color": "black",
            "textAlign": "center",
            "height": "auto",
            "whiteSpace": "normal",
        },
        style_data={
            "whiteSpace": "normal",
            "height": "auto",
            "lineHeight": "15px",
            "width": "100px",
            "maxWidth": "100px",
            "minWidth": "100px",
        },
    ),

    actions = ["Minimax Scaler", "Standard Scaler", "Robust Scaler", "Gaussian Scaler"]
    action_list = [{"label": str(act), "value": str(act)} for act in actions]
    # Create a list of dictionaries of features
    numerics = []
    for col in df.columns:
        if df[col].describe().size == 8:
            numerics.append(col)

    columns = [{"label": str(col), "value": str(col)} for col in numerics]

    layout = dbc.Container(
        [
            help,
            help_button,
            home_button,
            modal_norm_success,
            html.Div(upload_id, id="upload_id", style={"display": "none"}),
            dbc.Card(
                [
                    # Headline of page
                    dbc.Row(
                        dbc.Col(html.H2("Normalization"), width={"size": "auto"}),
                        justify="center",
                    ),
                    dbc.Row(
                        [
                            dbc.Col(
                                [
                                    dbc.Label("Column"),
                                    dcc.Dropdown(
                                        id="id_column_norm",
                                        options=columns,
                                        placeholder="Select a column",
                                        style={"color": "black"},
                                        multi=True
                                    ),
                                ],
                                width={"size": 5},
                            ),
                            dbc.Col(
                                [
                                    # Dropdown for selecting year
                                    dbc.Label("Method"),
                                    dcc.Dropdown(
                                        id="id_method_norm",
                                        options=action_list,
                                        placeholder="Select an action",
                                        style={"color": "black"},
                                        multi=False,
                                    ),
                                ],
                                width={"size": 5},
                            ),
                        ],
                        justify="center",
                    ),
                    dbc.Row([
                        dbc.Col(
                            [
                                html.Br(),
                                dbc.Button(
                                    "Submit",
                                    color="success",
                                    className="mr-1",
                                    n_clicks=0,
                                    id="id_submit",
                                ),
                            ],
                            width={"size": "auto"},
                        ),
                    ],
                        justify="center",
                    )
                ],
                body=True,
                style={"background-color": "white", "border-color": "#A6AFFF"}
            ),
            # Graph component for showing result in form of a scatter plot
            dbc.Card(
                dbc.Row(
                    dbc.Col(
                        info_table,
                        width={"size": 11},
                    ),
                    justify="center",
                ),
                body=True,
                style={"margin-top": "1rem", "background-color": "white", "border-color": "#A6AFFF"},
            ),
        ],
        fluid=True,
    )

    return layout


# --------------- Functions for Callback Section -------------

@app.callback(
    Output("info_table_norm", "data"),
    Output("modal_norm_success", "is_open"),
    Input("id_submit", "n_clicks"),
    State("id_method_norm", "value"),
    State("id_column_norm", "value"),
    State("upload_id", "children"),
    prevent_initial_call=True,
)
def apply_method(click, method, columns, upload_id):
    # If only one group was selected, it would be converted to form of a list
    if not isinstance(columns, list):
        columns = [columns]
    if len(columns) == 0 or method == None:
        raise PreventUpdate
    filename = os.listdir(FILEPATH + upload_id)[0]
    df = pd.read_csv(FILEPATH + upload_id + '/' + filename)

    if method == "Minimax Scaler":
        scaler = MinMaxScaler()
    elif method == "Standard Scaler":
        scaler = StandardScaler()
    elif method == "Robust Scaler":
        scaler = RobustScaler()
    elif method == "Gaussian Scaler":
        scaler = PowerTransformer()

    # Scale and return a numpy array
    X_scaled = scaler.fit_transform(df[columns])

    # Convert array to dataframe with same columns
    temp_df = pd.DataFrame(X_scaled, columns=columns)
    df[columns] = temp_df

    # save changes to file and return success modal
    df.to_csv(FILEPATH + upload_id + "/" + filename, encoding="utf_8_sig", index=False)
    return info_provider(df), True

@app.callback(
    Output("normalization_help", "is_open"),
    Input("help_button", "n_clicks"),
    prevent_initial_call=True,
)
def open_help(clck):
    return True