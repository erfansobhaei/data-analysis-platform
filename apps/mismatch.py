import dash_bootstrap_components as dbc
import os
import pandas as pd
import plotly.express as px
import plotly.figure_factory as ff
from dash import callback_context
from dash import dcc
from dash import html
from dash.dependencies import Input, Output, State
import numpy as np
from app import app


FILEPATH = "./.var/uploads/"

HIDE = {
    "display": "none",
}
df = pd.DataFrame()

home_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-home-96.png", style={"width": "50%"})], href="/", className="home_button")])

help_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-question-mark-96.png", style={"width": "50%"})], className="help_button", id="help_button")])

help = dbc.Offcanvas(
            html.P([
                "Here you can find a tutorial on how to use this page",
                "Here is a usefull ",
                html.A("link", href="google.com")]
            ),
            id="mismatch_help",
            title="Tutorial",
            is_open=False,
            close_button=True,
            placement="end"
        )

def layout(df1, upload_id):
    # Read data from csv file
    df = df1.copy()

    type_list = ["Integer", "Float", "String", "Boolean"]
    all_types = [{"label": str(item), "value": str(item)} for item in type_list]
    # Create a list of dictionaries of features
    columns = [{"label": str(col), "value": str(col)} for col in df.columns]

    layout = dbc.Container(
        [
            help,
            home_button,
            help_button,
            html.Div(upload_id, id="upload_id", style=HIDE),
            dbc.Card(
                [
                    # Headline of page
                    dbc.Row(
                        dbc.Col(html.H2("Mismatch Data"), width={"size": "auto"}),
                        justify="center",
                    ),
                    dbc.Row(
                        [
                            dbc.Col(
                                [
                                    # Dropdown for selecting year
                                    dbc.Label("Choose Columns:"),
                                    dcc.Dropdown(
                                        id="column",
                                        options=columns,
                                        value=[str(col) for col in df.columns],
                                        multi=True,
                                        placeholder="Select a Column",
                                        style={"color": "black"},
                                    ),
                                ],
                                width={"size": 7},
                            ),
                            dbc.Col(
                                [
                                    dbc.Label("Choose type to remove:"),
                                    dcc.Dropdown(
                                        id="id_type_mismatch",
                                        options=all_types,
                                        placeholder="Select a Type",
                                        style={"color": "black"},
                                        multi=True,
                                    ),
                                ],
                                width={"size": 5},
                            ),
                        ],
                        justify="center",
                    ),
                    dbc.Row([
                        dbc.Col(
                            [
                                html.Br(),
                                dbc.Button(
                                    "Submit",
                                    color="success",
                                    className="mr-1",
                                    n_clicks=0,
                                    id="id_submit",
                                ),
                            ],
                            width={"size": "auto"},
                        ),
                    ],
                        justify="center",
                    )
                ],
                body=True,
                style={"background-color": "white", "border-color": "#A6AFFF"}
            ),
            # Graph component for showing result in form of a scatter plot
            dbc.Card(
                html.Div(id="container_mismatch", children=[]),
                body=True,
                style={"margin-top": "1rem", "background-color": "white", "border-color": "#A6AFFF"},
            ),
        ],
        fluid=True,
    )

    return layout


def check_int(x):
    return str(x).isdigit()


def check_float(x):
    try:
        if "." in str(x):
            float(x)
            return True
        return False
    except ValueError:
        return False


def check_bool(x):
    return str(x) == "False" or str(x) == "True"


def check_str(x):
    is_int = check_int(x)
    is_float = check_float(x)
    is_bool = check_bool(x)
    return not (is_int or is_float or is_bool)


def count_types(df):
    result = []
    for col in df:
        col_notnull = df[df[col].notnull()][col]
        types_frequency = {
            "column": col,
            "int": col_notnull.apply(check_int).sum(),
            "float": col_notnull.apply(check_float).sum(),
            "bool": col_notnull.apply(check_bool).sum(),
            "str": col_notnull.apply(check_str).sum(),
            "nan": df[col].isnull().sum(),
        }

        result.append(types_frequency)
    return result

def remove_type(df, columns, types_to_remove):
    if types_to_remove == []:
        return df
    for col in columns:
        for row in range(len(df[col])):
            if "Integer" in types_to_remove:
                if check_int(df[col][row]):
                    df[col][row] = np.nan
            if "Float" in types_to_remove:
                if check_float(df[col][row]):
                    df[col][row] = np.nan
            if "String" in types_to_remove:
                if check_str(df[col][row]):
                    df[col][row] = np.nan
            if "Boolean" in types_to_remove:
                if check_bool(df[col][row]):
                    df[col][row] = np.nan
    return df


@app.callback(
    Output("container_mismatch", "children"),
    Input("id_submit", "n_clicks"),
    State("column", "value"),
    State("id_type_mismatch", "value"),
    State("upload_id", "children"),
)
def update_cards(clck0, columns, types_to_remove, upload_id):
    filename = os.listdir(FILEPATH + upload_id)[0]
    df = pd.read_csv(FILEPATH + upload_id + '/' + filename)
    if not isinstance(columns, list):
        columns = [columns]
    if not isinstance(types_to_remove, list):
        types_to_remove = [types_to_remove]

    df = remove_type(df, columns, types_to_remove)

    types = count_types(df[columns])

    children = []
    
    for item in types:
        rows = []
        for label, value in item.items():
            rows.append(
                dbc.Row(
                    dbc.Label(f"{label}: {value}", style={"text-align": "center"}),
                    align="center"
                )
            )
        pie = [str(value) for label, value in item.items()]
        card = dbc.Card([
            dbc.Row(
                dbc.Col(html.H4(item["column"]), width={"size": "auto"}),
                justify="center",
                style={"margin-top": "1rem"}
            ),
            dbc.Row([
                dbc.Col(
                    dcc.Loading(
                        children=[dcc.Graph(id="pie_plot", figure=px.pie(
                            names=["int", "float", "bool", "str", "nan"],
                            values=pie[1:],
                            hole=.3,
                            template="plotly"))],
                        color="#BDD5EF",
                        type="cube",
                        fullscreen=False,
                    ),
                    width=8),
                dbc.Col(rows, width=4, align="center")],
                style={"display": "flex", "justify-content": "center", "align-items": "center"})
        ], style={"background-color": "white", "border-color": "#A6AFFF", "margin-bottom": "1rem"})
        children.append(card)
        df.to_csv(FILEPATH + upload_id + "/" + filename, encoding="utf_8_sig", index=False)
    return children

@app.callback(
    Output("mismatch_help", "is_open"),
    Input("help_button", "n_clicks"),
    prevent_initial_call=True,
)
def open_help(clck):
    return True