import os

import dash_bootstrap_components as dbc
import pandas as pd
from dash import callback_context
from dash import dash_table
from dash import dcc
from dash import html
from dash.dependencies import Input, Output, State

from app import app

FILEPATH = "./.var/uploads/"

df = pd.DataFrame()
home_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-home-96.png", style={"width": "50%"})], href="/", className="home_button")])

help_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-question-mark-96.png", style={"width": "50%"})], className="help_button", id="help_button")])

help = dbc.Offcanvas(
            html.P([
                "Here you can find a tutorial on how to use this page",
                "Here is a usefull ",
                html.A("link", href="google.com")]
            ),
            id="edit_help",
            title="Tutorial",
            is_open=False,
            close_button=True,
            placement="end"
        )

def layout(df1, upload_id):
    # Read data from csv file
    global df
    df = df1.copy()
    df_target = df.copy()

    # Create a list of dictionaries in form of {'label':'2021', 'value': 2021}
    groups = [{"label": str(col), "value": str(col)} for col in df.columns]

    # Create a list of dictionaries of features
    features = [
        {"label": str(col), "value": str(col)}
        for col in df.select_dtypes(include="number")
    ]

    layout = dbc.Container(
        [
            help,
            help_button,

            home_button,
            modal_delete_success,
            modal_edit_success,
            html.Div(upload_id, id="upload_id", style={"display": "none"}),
            # Headline of page
            dbc.Card(
                [
                    dbc.Row(
                        dbc.Col(html.H2("Data Table"), width={"size": "auto"}),
                        justify="center",
                    ),
                    dbc.Row(
                        dbc.Col(
                            [
                                dash_table.DataTable(
                                    id="datatable_edit",
                                    columns=[
                                        {
                                            "name": i,
                                            "id": i,
                                            "deletable": True,
                                            "selectable": True,
                                            "editable": True,
                                        }
                                        for i in df.columns
                                    ],
                                    data=df.to_dict("records"),
                                    editable=True,
                                    filter_action="native",
                                    sort_action="native",
                                    sort_mode="single",
                                    column_selectable="multi",
                                    selected_columns=[],
                                    selected_rows=[],
                                    page_action="native",
                                    page_current=0,
                                    style_table={"overflowX": "auto"},
                                    page_size=20,
                                    row_selectable="multi",
                                    row_deletable=True,
                                    style_header={
                                        "color": "black",
                                    },
                                    style_cell={
                                        "color": "black",
                                        "textAlign": "center",
                                        "height": "auto",
                                        "whiteSpace": "normal",
                                    },
                                    style_data={
                                        "whiteSpace": "normal",
                                        "height": "auto",
                                        "lineHeight": "15px",
                                        "width": "100px",
                                        "maxWidth": "50px",
                                        "minWidth": "200px",
                                    },
                                ),
                            ],
                            width={"size": "auto"},
                        ),
                        justify="center",
                    ),
                    dbc.Row([
                        dbc.Col(
                            [
                                html.Br(),
                                dbc.Button(
                                    "Submit Changes",
                                    color="success",
                                    className="mr-1",
                                    n_clicks=0,
                                    id="id_submit",
                                ),
                            ],
                            width={"size": "auto"},
                        ),
                        dbc.Col(
                            [
                                html.Br(),
                                dbc.Button(
                                    "Delete Selected",
                                    color="danger",
                                    className="mr-1",
                                    n_clicks=0,
                                    id="delete_sel",
                                ),
                            ],
                            width={"size": "auto"},
                        ),
                        dbc.Col(
                            [
                                html.Br(),
                                dbc.Button(
                                    "Remove Duplicate",
                                    color="secondary",
                                    className="mr-1",
                                    n_clicks=0,
                                    id="remove_duplicate",
                                ),
                            ],
                            width={"size": "auto"},
                        ),
                        dbc.Col(
                            [
                                html.Br(),
                                dbc.Button(
                                    "Download",
                                    color="primary",
                                    className="mr-1",
                                    n_clicks=0,
                                    id="download_table",
                                ),
                            ],
                            width={"size": "auto"},
                        ),
                    ],
                        justify="center",
                    )
                ],
                body=True,
                style={"margin-top": "1rem", "background-color": "white", "border-color": "#A6AFFF"},
            ),
            dcc.Download(id="export_table")
        ],
        fluid=True,
    )
    return layout


modal_edit_success = html.Div(
    [
        dbc.Modal(
            [
                dbc.ModalHeader(html.H5("Success!", style={"font-size": "25px"})),
                html.Br(),
                dbc.ModalBody("New changes have been applied to your dataset."),
                html.Br(),
            ],
            id="modal_edit_success",
            is_open=False,
        ),
    ])
modal_delete_success = html.Div(
    [
        dbc.Modal(
            [
                dbc.ModalHeader(html.H5("Done!", style={"font-size": "25px"})),
                html.Br(),
                dbc.ModalBody("The changes have been succesfully applied"),
                html.Br(),
            ],
            id="modal_delete_success",
            is_open=False,
        ),
    ])


@app.callback(
    Output("modal_edit_success", "is_open"),
    Input("id_submit", "n_clicks"),
    State("datatable_edit", "columns"),
    State("datatable_edit", "data"),
    State("upload_id", "children"),
    prevent_initial_call=True,
)
def apply_changes(clck, new_col, new_data, uid):
    cols = []
    for col in new_col:
        cols.append(col["name"])
    editted_df = pd.DataFrame(new_data)
    editted_df = editted_df[cols]
    filename = os.listdir(FILEPATH + uid)[0]
    editted_df.to_csv(FILEPATH + uid + "/" + filename, encoding="utf_8_sig", index=False)
    return True


@app.callback(
    Output("modal_delete_success", "is_open"),
    Output("datatable_edit", "columns"),
    Output("datatable_edit", "data"),
    Output("datatable_edit", "selected_columns"),
    Output("datatable_edit", "selected_rows"),
    Input("remove_duplicate", "n_clicks"),
    Input("delete_sel", "n_clicks"),
    State("datatable_edit", "columns"),
    State("datatable_edit", "data"),
    State("datatable_edit", "selected_columns"),
    State("datatable_edit", "selected_rows"),
    State("upload_id", "children"),
    prevent_initial_call=True,
)
def delete_selected(clck0, clck1, all_cols, all_data, sel_cols, sel_rows, uid):
    ctx = callback_context
    if not ctx.triggered:
        button_id = 'delete_sel'
    else:
        button_id = ctx.triggered[0]['prop_id'].split('.')[0]
    if button_id == "delete_sel":
        cols = []
        for col in all_cols:
            cols.append(col["name"])
        editted_df = pd.DataFrame(all_data)
        editted_df = editted_df[cols]
        remaining = [x for x in editted_df.columns if x not in sel_cols]
        editted_df = editted_df[remaining]
        for i in sel_rows:
            editted_df = editted_df.drop(i)
        filename = os.listdir(FILEPATH + uid)[0]
        new_columns = [
            {
                "name": i,
                "id": i,
                "deletable": True,
                "selectable": True,
                "editable": True,
            } for i in editted_df.columns
        ]
        new_data = editted_df.to_dict("records")
        editted_df = editted_df.reset_index(drop=True)
        editted_df.to_csv(FILEPATH + uid + "/" + filename, encoding="utf_8_sig", index=False)
        return True, new_columns, new_data, [], []
    elif button_id == "remove_duplicate":
        cols = []
        for col in all_cols:
            cols.append(col["name"])
        editted_df = pd.DataFrame(all_data)
        editted_df = editted_df[cols]
        editted_df = editted_df.drop_duplicates()
        filename = os.listdir(FILEPATH + uid)[0]
        new_columns = [
            {
                "name": i,
                "id": i,
                "deletable": True,
                "selectable": True,
                "editable": True,
            } for i in editted_df.columns
        ]
        new_data = editted_df.to_dict("records")
        editted_df = editted_df.reset_index(drop=True)
        editted_df.to_csv(FILEPATH + uid + "/" + filename, encoding="utf_8_sig", index=False)
        return True, new_columns, new_data, [], []


@app.callback(
    Output("export_table", "data"),
    Input("download_table", "n_clicks"),
    State("datatable_edit", "columns"),
    State("datatable_edit", "data"),
    State("upload_id", "children"),
    prevent_initial_call=True,
)
def export_data(n_clicks, all_cols, data, uid):
    cols = []
    for col in all_cols:
        cols.append(col["name"])
    download = pd.DataFrame(data)
    download = download[cols]
    filename = os.listdir(FILEPATH + uid)[0]
    return dcc.send_data_frame(download.to_csv, filename, index=False, encoding="utf-8-sig")

@app.callback(
    Output("edit_help", "is_open"),
    Input("help_button", "n_clicks"),
    prevent_initial_call=True,
)
def open_help(clck):
    return True