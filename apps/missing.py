import os
import dash_bootstrap_components as dbc
import pandas as pd
from dash import dash_table
from dash import dcc
from dash import html
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
from sklearn.impute import KNNImputer
from app import app

FILEPATH = "./.var/uploads/"
HIDE = {
    "display": "none",
}
home_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-home-96.png", style={"width": "50%"})], href="/", className="home_button")])

modal_miss_success = html.Div(
    [
        dbc.Modal(
            [
                dbc.ModalHeader(html.H5("Success!", style={"font-size": "25px"})),
                html.Br(),
                dbc.ModalBody("Selected action has been applied to the selected columns"),
                html.Br(),
            ],
            id="modal_miss_success",
            is_open=False,
        ),
    ])

help_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-question-mark-96.png", style={"width": "50%"})], className="help_button",
            id="help_button")])

help = dbc.Offcanvas(
    html.P([
        "Here you can find a tutorial on how to use this page",
        "Here is a usefull ",
        html.A("link", href="google.com")]
    ),
    id="missing_help",
    title="Tutorial",
    is_open=False,
    close_button=True,
    placement="end"
)


def info_provider(df):
    tdata = []
    for column in df.columns:
        info = {}
        if df[column].describe().size == 8:
            info["Column"] = column
            info["Count"] = df[column].describe()["count"]
            info["Missing"] = df[column].isnull().sum()
            info["Mean"] = df[column].describe()["mean"]
            info["STD"] = df[column].describe()["std"]
            info["Min"] = df[column].describe()["min"]
            info["Max"] = df[column].describe()["max"]
            info["Unique"] = "-"
        else:
            info["Column"] = column
            info["Count"] = df[column].describe()["count"]
            info["Missing"] = df[column].isnull().sum()
            info["Mean"] = "-"
            info["STD"] = "-"
            info["Min"] = "-"
            info["Max"] = "-"
            info["Unique"] = df[column].describe()["unique"]
        tdata.append(info)
    return tdata


def layout(df1, upload_id):
    # Read data from csv file
    df = df1.copy()

    tdata = info_provider(df)

    info_table = dash_table.DataTable(
        id="info_table_miss",
        columns=[
            {
                "name": i,
                "id": i,
            }
            for i in ["Column", "Count", "Missing", "Mean", "STD", "Min", "Max", "Unique"]
        ],
        data=tdata,
        sort_action="native",
        sort_mode="single",
        page_action="native",
        page_current=0,
        style_table={"overflowX": "auto"},
        page_size=20,
        style_header={
            "color": "black",
        },
        style_cell={
            "color": "black",
            "textAlign": "center",
            "height": "auto",
            "whiteSpace": "normal",
        },
        style_data={
            "whiteSpace": "normal",
            "height": "auto",
            "lineHeight": "15px",
            "width": "100px",
            "maxWidth": "50px",
            "minWidth": "100px",
        },
    ),

    actions = ["Remove Rows With Missing Data", "Remove Column", "Static Value", "LOCF", "Interpolation", "Mean",
               "Median", "KNN"]
    action_list = [{"label": str(act), "value": str(act)} for act in actions]
    # Create a list of dictionaries of features
    columns = [{"label": str(col), "value": str(col)} for col in df.columns]

    features = [
        {"label": str(col), "value": str(col)}
        for col in df.select_dtypes(include="number")
    ]

    layout = dbc.Container(
        [
            help,
            help_button,
            home_button,
            modal_miss_success,
            html.Div(upload_id, id="upload_id", style={"display": "none"}),
            dbc.Card(
                [
                    # Headline of page
                    dbc.Row(
                        dbc.Col(html.H2("Missing Values"), width={"size": "auto"}),
                        justify="center",
                    ),
                    dbc.Row(
                        [
                            dbc.Col(
                                [
                                    dbc.Label("Column"),
                                    dcc.Dropdown(
                                        id="id_column_miss",
                                        options=columns,
                                        placeholder="Select columns",
                                        style={"color": "black"},
                                        multi=True,
                                    ),
                                ],
                                width={"size": 5},
                            ),
                            dbc.Col(
                                [
                                    dbc.Label("Method"),
                                    dcc.Dropdown(
                                        id="id_method_miss",
                                        options=action_list,
                                        placeholder="Select an action",
                                        style={"color": "black"},
                                        multi=False,
                                    ),
                                ],
                                width={"size": 5},
                            ),
                        ],
                        justify="center",
                        style={"margin-bottom": "0.5rem"}
                    ),
                    dbc.Row(
                        [
                            dbc.Col(
                                [
                                    dbc.Label("Static Value"),
                                    dcc.Input(
                                        id="static_value_miss",
                                        placeholder="Please enter a value to replace it with missing values",
                                        type="text",
                                        style={"cursor": "text"},
                                        className="Select-control",
                                    ),
                                ],
                                width={"size": 5},
                                id="static_input_miss",
                                style=HIDE,
                            ),
                            dbc.Col(
                                [
                                    dbc.Label("Predictors"),
                                    dcc.Dropdown(
                                        id="knn_value_miss_feat",
                                        options=features,
                                        placeholder="Select columns",
                                        style={"color": "black"},
                                        multi=True,
                                    ),
                                ],
                                width={"size": 4},
                                id="knn_input_miss_feat",
                                style=HIDE,
                            ),
                            dbc.Col(
                                [
                                    dbc.Label("Number of neighbours"),
                                    dcc.Input(
                                        id="knn_value_miss",
                                        type="number",
                                        min=1,
                                    ),
                                ],
                                width={"size": 2},
                                id="knn_input_miss",
                                style=HIDE,
                            ),
                        ],
                        justify="center",
                        id="conditional_input_container",
                        style=HIDE,
                    ),
                    dbc.Row([
                        dbc.Col(
                            [
                                html.Br(),
                                dbc.Button(
                                    "Submit",
                                    color="success",
                                    className="mr-1",
                                    id="id_submit",
                                ),
                            ],
                            width={"size": "auto"},
                        ),
                    ],
                        justify="center",
                    )
                ],
                body=True,
                style={"background-color": "white", "border-color": "#A6AFFF"}
            ),
            # Graph component for showing result in form of a scatter plot
            dbc.Card(
                dbc.Row(
                    dbc.Col(
                        info_table,
                        width={"size": 11},
                    ),
                    justify="center",
                ),
                body=True,
                style={"margin-top": "1rem", "background-color": "white", "border-color": "#A6AFFF"},
            ),
        ],
        fluid=True,
    )

    return layout


# --------------- Functions for Callback Section -------------

@app.callback(
    Output("info_table_miss", "data"),
    Output("modal_miss_success", "is_open"),
    Input("id_submit", "n_clicks"),
    State("id_method_miss", "value"),
    State("id_column_miss", "value"),
    State("static_value_miss", "value"),
    State("knn_value_miss", "value"),
    State("knn_value_miss_feat", "value"),
    State("upload_id", "children"),
    prevent_initial_call=True,
)
def apply_method(click, method, columns, static, n_neighbors, knn_feat, upload_id):
    # If only one group was selected, it would be converted to form of a list
    if not isinstance(columns, list):
        columns = [columns]
    if len(columns) == 0 or method == None:
        raise PreventUpdate
    filename = os.listdir(FILEPATH + upload_id)[0]
    df = pd.read_csv(FILEPATH + upload_id + '/' + filename)

    knn_feat = knn_feat + columns

    for column in columns:
        if method == "Static Value":
            df[column] = df[column].fillna(static)
        elif method == "LOCF":
            df[column] = df[column].fillna(method="ffill")
        elif method == "Mean":
            df[column] = df[column].fillna(float(df[column].mean()))
        elif method == "Median":
            df[column] = df[column].fillna(float(df[column].median()))
        elif method == "Interpolation":
            df[column] = df[column].interpolate(method='linear')
        elif method == "KNN":
            print(knn_feat)
            imputer = KNNImputer(n_neighbors=n_neighbors)
            X_imputed = imputer.fit_transform(df[knn_feat])
            X_imputed = pd.DataFrame(X_imputed, columns=knn_feat)
            df[column] = X_imputed[column]

    if method == "Remove Rows With Missing Data":
        df = df.dropna(subset=columns)
    elif method == "Remove Column":
        df = df.drop(columns=columns)

    df.to_csv(FILEPATH + upload_id + "/" + filename, encoding="utf_8_sig", index=False)
    return info_provider(df), True


@app.callback(
    Output("conditional_input_container", "style"),
    Output("static_input_miss", "style"),
    Output("knn_input_miss", "style"),
    Output("knn_input_miss_feat", "style"),
    Input("id_method_miss", "value"),
    prevent_initial_call=True,
)
def show_static(val):
    if val == "Static Value":
        return {}, {}, HIDE, HIDE
    elif val == "KNN":
        return {}, HIDE, {}, {}
    else:
        return HIDE, HIDE, HIDE, HIDE


@app.callback(
    Output("id_method_miss", "options"),
    Input("id_column_miss", "value"),
    State("upload_id", "children"),
    prevent_initial_call=True,
)
def show_methods(columns, upload_id):
    files = os.listdir(FILEPATH + upload_id)
    df = pd.read_csv(FILEPATH + upload_id + '/' + files[0])
    is_cat = False
    if not isinstance(columns, list):
        columns = [columns]
    for column in columns:
        if df[column].describe().size == 8:
            is_cat = False
        else:
            is_cat = True
            break
    if is_cat == True:
        actions = ["Remove Rows With Missing Data", "Remove Column", "Static Value"]
        action_list = [{"label": str(act), "value": str(act)} for act in actions]
        return action_list
    else:
        actions = ["Remove Rows With Missing Data", "Remove Column", "Static Value", "LOCF", "Interpolation", "Mean",
                   "Median", "KNN"]
        action_list = [{"label": str(act), "value": str(act)} for act in actions]
        return action_list


@app.callback(
    Output("missing_help", "is_open"),
    Input("help_button", "n_clicks"),
    prevent_initial_call=True,
)
def open_help(clck):
    return True
