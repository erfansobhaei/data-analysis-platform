import dash_bootstrap_components as dbc
import pandas as pd
import plotly.express as px
import plotly.figure_factory as ff
from dash import callback_context
from dash import dcc
from dash import html
from dash.dependencies import Input, Output, State

from app import app

df = pd.DataFrame()

home_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-home-96.png", style={"width": "50%"})], href="/", className="home_button")])

help_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-question-mark-96.png", style={"width": "50%"})], className="help_button", id="help_button")])

help = dbc.Offcanvas(
            html.P([
                "Here you can find a tutorial on how to use this page",
                "Here is a usefull ",
                html.A("link", href="google.com")]
            ),
            id="summary_help",
            title="Tutorial",
            is_open=False,
            close_button=True,
            placement="end"
        )

def layout(df1):
    # Read data from csv file
    global df
    df = df1.copy()

    # Create a list of dictionaries of features
    columns = [{"label": str(col), "value": str(col)} for col in df.columns]

    layout = dbc.Container(
        [
            help,
            home_button,
            help_button,
            dbc.Card(
                [
                    # Headline of page
                    dbc.Row(
                        dbc.Col(html.H2("Summary"), width={"size": "auto"}),
                        justify="center",
                    ),
                    dbc.Row(
                        [
                            dbc.Col(
                                [
                                    # Dropdown for selecting year
                                    dbc.Label("Choose Your Columns:"),
                                    dcc.Dropdown(
                                        id="column",
                                        options=columns,
                                        value=[str(col) for col in df.columns],
                                        multi=True,
                                        placeholder="Select a feature",
                                        style={"color": "black"},
                                    ),
                                ],
                                width={"size": 7},
                            ),
                        ],
                        justify="center",
                    ),
                    dbc.Row([
                        dbc.Col(
                            [
                                html.Br(),
                                dbc.Button(
                                    "Distributions",
                                    color="success",
                                    className="mr-1",
                                    n_clicks=0,
                                    id="id_submit",
                                ),
                            ],
                            width={"size": "auto"},
                        ),
                        dbc.Col(
                            [
                                html.Br(),
                                dbc.Button(
                                    "Correlation Matrix",
                                    color="info",
                                    className="mr-1",
                                    n_clicks=0,
                                    id="corr_mat",
                                ),
                            ],
                            width={"size": "auto"},
                        ),
                        dbc.Col(
                            [
                                html.Br(),
                                dbc.Button(
                                    "Data Types",
                                    color="primary",
                                    className="mr-1",
                                    n_clicks=0,
                                    id="id_type",
                                ),
                            ],
                            width={"size": "auto"},
                        )
                    ],
                        justify="center",
                    )
                ],
                body=True,
                style={"background-color": "white", "border-color": "#A6AFFF"}
            ),
            # Graph component for showing result in form of a scatter plot
            dbc.Card(
                html.Div(id="container", children=[]),
                body=True,
                style={"margin-top": "1rem", "background-color": "white", "border-color": "#A6AFFF"},
            ),
        ],
        fluid=True,
    )

    return layout


def check_int(x):
    return str(x).isdigit()


def check_float(x):
    try:
        if "." in str(x):
            float(x)
            return True
        return False
    except ValueError:
        return False


def check_bool(x):
    return str(x) == "False" or str(x) == "True"


def check_str(x):
    is_int = check_int(x)
    is_float = check_float(x)
    is_bool = check_bool(x)
    return not (is_int or is_float or is_bool)


def count_types(df):
    result = []
    for col in df:
        col_notnull = df[df[col].notnull()][col]
        types_frequency = {
            "column": col,
            "int": col_notnull.apply(check_int).sum(),
            "float": col_notnull.apply(check_float).sum(),
            "bool": col_notnull.apply(check_bool).sum(),
            "str": col_notnull.apply(check_str).sum(),
            "nan": df[col].isnull().sum(),
        }

        result.append(types_frequency)
    return result


@app.callback(
    Output("container", "children"),
    Input("id_submit", "n_clicks"),
    Input("corr_mat", "n_clicks"),
    Input("id_type", "n_clicks"),
    State("column", "value"),
)
def update_cards(clck0, clck1, clck2, columns):
    ctx = callback_context
    if not ctx.triggered:
        button_id = 'id_submit'
    else:
        button_id = ctx.triggered[0]['prop_id'].split('.')[0]
    if button_id == "id_submit":
        children = []
        for column in columns:
            info = df[column].describe()
            rows = []
            for label, value in info.items():
                if label == 'count':
                    value = int(value)
                elif type(value) == float:
                    value = round(value, 2)
                rows.append(
                    dbc.Row(
                        dbc.Label(f"{label}: {value}", style={"text-align": "center"}),
                        align="center"
                    )
                )
            rows.append(
                dbc.Row(
                    dbc.Label(f"Missing Values: {df[column].isnull().sum()}", style={"text-align": "center"}),
                    align="center"
                )
            )
            card = dbc.Card([
                dbc.Row(
                    dbc.Col(html.H4(column), width={"size": "auto"}),
                    justify="center",
                    style={"margin-top": "1rem"}
                ),
                dbc.Row([
                    dbc.Col(
                        dcc.Loading(
                            children=[dcc.Graph(id="summary_plot", figure=px.histogram(
                                df[column],
                                marginal="box",
                                template="plotly"))],
                            color="#BDD5EF",
                            type="cube",
                            fullscreen=False,
                        ),
                        width=8),
                    dbc.Col(rows, width=4, align="center")],
                    style={"display": "flex", "justify-content": "center", "align-items": "center"})
            ], style={"background-color": "white", "border-color": "#A6AFFF", "margin-bottom": "1rem"})
            children.append(card)
        return children
    elif button_id == "corr_mat":
        children = []
        if not isinstance(columns, list):
            columns = [columns]
        dff = df[columns]
        corr = dff.corr()
        z = corr.values.tolist()
        z_text = [[str(round(y, 3)) for y in x] for x in z]

        fig = ff.create_annotated_heatmap(z, x=list(corr.columns),
                                          y=list(corr.columns),
                                          annotation_text=z_text, colorscale='matter')

        # add custom xaxis title
        fig.add_annotation(dict(font=dict(color="black", size=12),
                                x=0.5,
                                y=-0.15,
                                showarrow=False,
                                text="",
                                xref="paper",
                                yref="paper"))

        # add custom yaxis title
        fig.add_annotation(dict(font=dict(color="black", size=12),
                                x=-0.35,
                                y=0.5,
                                showarrow=False,
                                text="",
                                textangle=-90,
                                xref="paper",
                                yref="paper"))

        # adjust margins to make room for yaxis title
        fig.update_layout(margin=dict(t=50, l=200))

        # add colorbar
        fig['data'][0]['showscale'] = True
        card = dbc.Card([
            dbc.Row(
                dbc.Col(html.H4("Correlation Matrix"), width={"size": "auto"}),
                justify="center",
                style={"margin-top": "1rem"}
            ),
            dbc.Row([
                dbc.Col(
                    dcc.Loading(
                        children=[dcc.Graph(id="corr_plot", figure=fig)],
                        color="#BDD5EF",
                        type="cube",
                        fullscreen=False,
                    ),
                    width=8),
            ], style={"display": "flex", "justify-content": "center", "align-items": "center"})
        ], style={"background-color": "white", "border-color": "#A6AFFF", "margin-bottom": "1rem"})
        return card
    elif button_id == "id_type":
        if not isinstance(columns, list):
            columns = [columns]
        types = count_types(df[columns])
        children = []
        for item in types:
            rows = []
            for label, value in item.items():
                rows.append(
                    dbc.Row(
                        dbc.Label(f"{label}: {value}", style={"text-align": "center"}),
                        align="center"
                    )
                )
            pie = [str(value) for label, value in item.items()]
            card = dbc.Card([
                dbc.Row(
                    dbc.Col(html.H4(item["column"]), width={"size": "auto"}),
                    justify="center",
                    style={"margin-top": "1rem"}
                ),
                dbc.Row([
                    dbc.Col(
                        dcc.Loading(
                            children=[dcc.Graph(id="pie_plot", figure=px.pie(
                                names=["int", "float", "bool", "str", "nan"],
                                values=pie[1:],
                                hole=.3,
                                template="plotly"))],
                            color="#BDD5EF",
                            type="cube",
                            fullscreen=False,
                        ),
                        width=8),
                    dbc.Col(rows, width=4, align="center")],
                    style={"display": "flex", "justify-content": "center", "align-items": "center"})
            ], style={"background-color": "white", "border-color": "#A6AFFF", "margin-bottom": "1rem"})
            children.append(card)
        return children

@app.callback(
    Output("summary_help", "is_open"),
    Input("help_button", "n_clicks"),
    prevent_initial_call=True,
)
def open_help(clck):
    return True