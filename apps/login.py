import dash_bootstrap_components as dbc
from dash import dcc
from dash import html

FILEPATH = "./.var/uploads/"
HIDE = {
    "display": "none",
}


def layout():
    layout = dbc.Container(
        [
            dbc.Card(
                [
                    # Headline of page
                    dbc.Row(
                        dbc.Col(html.H2("Login"), width={"size": "auto"}),
                        justify="center",
                    ),
                    dbc.Row([
                        dbc.Col([
                            dbc.Label("Phone Number"),
                            dcc.Input(
                                id="login_value",
                                placeholder="09123456789",
                                type="tel",
                                style={"cursor": "text"},
                                className="Select-control",
                            ),
                            dbc.Label("Password"),
                            dcc.Input(
                                id="password_value",
                                placeholder="",
                                type="password",
                                style={"cursor": "text"},
                                className="Select-control",
                            )
                        ], width={"size": 3})
                    ],
                        justify="center",
                        style={"margin-bottom": "0.5rem"},
                    ),
                    dbc.Row([
                        dbc.Col(
                            [
                                html.Br(),
                                dbc.Button(
                                    "Sign In",
                                    color="success",
                                    className="mr-1",
                                    id="id_submit",
                                ),
                            ],
                            width={"size": "auto"},
                        ),
                    ],
                        justify="center",
                    )
                ],
                body=True,
                style={"background-color": "white", "border-color": "#A6AFFF", "margin": "30px"}
            ),
        ],
        fluid=True,
    )

    return layout

# --------------- Functions for Callback Section -------------
