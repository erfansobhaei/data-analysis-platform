import dash_bootstrap_components as dbc
from dash import dcc
from dash import html
from dash_iconify import DashIconify


FILEPATH = "./.var/uploads/"
HIDE = {
    "display": "none",
}

def card_creator(name, role, affilation, image, links):

    card = dbc.Card(
        [
            dbc.Row(
                [
                    dbc.Col(
                        dbc.CardImg(
                            src=image,
                            className="img-fluid rounded-start",
                        ),
                        className="col-md-4",
                    ),
                    dbc.Col(
                        dbc.CardBody(
                            [
                                html.H4(name, className="card-title"),
                                html.P(
                                    role,
                                    className="card-text",
                                ),
                                html.P(
                                    affilation,
                                    className="card-text",
                                ),
                                
                                dbc.Row([
                                    dbc.Col(html.A(DashIconify(icon="logos:linkedin-icon", width = 30, height=30), href=links[0]), width=2),
                                    dbc.Col(html.A(DashIconify(icon="logos:google-gmail", width = 30, height=30), href=links[1]), width=2),
                                    dbc.Col(html.A(DashIconify(icon="logos:twitter", width = 30, height=30), href=links[2]), width=2),
                                    dbc.Col(html.A(DashIconify(icon="icon-park:instagram", width = 30, height=30), href=links[3]), width=2),
                                    ],
                                    justify="start",
                                )
                            ]
                        ),
                        className="col-md-8",
                    ),
                ],
                className="g-0 d-flex align-items-center",
            )
        ],
        className="mb-3",
        style={"color": "black", "background": "#edefff"},
    )
    return card


def layout():
    links = ["http://linkedin.com", "http://gmail.com", "http://twitter.com", "http://instagram.com"]
    layout = dbc.Container(
        [
            dbc.Card(
                [
                    # Headline of page
                    dbc.Row(
                        dbc.Col(html.H3("About US"), width={"size": "auto"}),
                        justify="center",
                    ),
                    dbc.Row(
                        dbc.Col(html.H5("Get to know the Team that created DAP"), width={"size": "auto"}),
                        justify="center",
                    ),
                    dbc.Row(
                        dbc.Col(html.Br(), width={"size": "auto"}),
                        justify="center",
                    ),
                    dbc.Row([
                        dbc.Col(card_creator("Dr. Jamshid Shanbezadeh", "Project Leader", "Professor at Kharazmi University", "../assets/user.jpg", links)),
                        dbc.Col(card_creator("Ali Jarkani", "Project Manager", "Graduate Student at Kharazmi University", "../assets/user.jpg", links)),                        
                    ]),
                    dbc.Row([
                        dbc.Col(card_creator("Farbod Khodadadi", "Developer", "Undergraduate Student at Kharazmi University", "../assets/user.jpg", links)),
                        dbc.Col(card_creator("Erfan Sobhi", "Developer", "Undergraduate Student at Kharazmi University", "../assets/user.jpg", links)),                        
                        dbc.Col(card_creator("Mehdi Setak", "Developer", "Undergraduate Student at Kharazmi University", "../assets/user.jpg", links)),                        
                    ])
                    
                ],
                body=True,
                style={"background-color": "white", "border-color": "#A6AFFF", "margin": "30px"}
            ),
        ],
        fluid=True,
    )

    return layout
