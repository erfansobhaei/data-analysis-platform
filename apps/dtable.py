from pandas.core.algorithms import isin, mode
from dash import dash_table
import pandas as pd
import numpy as np
import plotly.express as px
from dash import dcc
from dash import html
import dash_daq as daq
from dash.exceptions import PreventUpdate
from dash.dependencies import Input, Output, State
from sklearn.cluster import KMeans
import dash_bootstrap_components as dbc
from app import app

df = pd.DataFrame()
home_button = html.Div([html.A([html.Img(src="../assets/icons8-home-96.png", style={"width":"50%"})],href="/", className="home_button")])

help_button = html.Div(
    [html.A([html.Img(src="../assets/icons8-question-mark-96.png", style={"width": "50%"})], className="help_button", id="help_button")])

help = dbc.Offcanvas(
            html.P([
                "Here you can find a tutorial on how to use this page",
                "Here is a usefull ",
                html.A("link", href="google.com")]
            ),
            id="dtable_help",
            title="Tutorial",
            is_open=False,
            close_button=True,
            placement="end"
        )

def layout(df1):
    # Read data from csv file
    global df
    df = df1.copy()
    df_target = df.copy()

    # Create a list of dictionaries in form of {'label':'2021', 'value': 2021}
    groups = [{"label": str(col), "value": str(col)} for col in df.columns]

    # Create a list of dictionaries of features
    features = [
        {"label": str(col), "value": str(col)}
        for col in df.select_dtypes(include="number")
    ]

    layout = dbc.Container(
        [
            help,
            help_button,
            
            home_button,
            # Headline of page
            dbc.Card(
                [
                    dbc.Row(
                        dbc.Col(html.H2("Data Table"), width={"size": "auto"}),
                        justify="center",
                    ),
                    dbc.Row(
                        dbc.Col(
                            [
                                dash_table.DataTable(
                                    id="datatable",
                                    columns=[
                                        {
                                            "name": i,
                                            "id": i,
                                        }
                                        for i in df.columns
                                    ],
                                    data=df.to_dict("records"),
                                    filter_action="native",
                                    sort_action="native",
                                    sort_mode="multi",
                                    page_action="native",
                                    page_current=0,
                                    style_table={"overflowX": "auto"},
                                    page_size=20,
                                    style_cell={
                                        "color": "black",
                                        "textAlign": "center",
                                        "height": "auto",
                                        "whiteSpace": "normal",
                                    },
                                    style_data={
                                        "whiteSpace": "normal",
                                        "height": "auto",
                                        "lineHeight": "15px",
                                        "width": "100px",
                                        "maxWidth": "50px",
                                        "minWidth": "200px",
                                    },
                                ),
                            ],
                            width={"size": "auto"},
                        ),
                        justify="center",
                    ),
                ],
                body=True,
                style={"margin-top": "1rem", "background-color":"white", "border-color":"#A6AFFF"},
            )
        ],
        fluid=True,
    )
    return layout

@app.callback(
    Output("dtable_help", "is_open"),
    Input("help_button", "n_clicks"),
    prevent_initial_call=True,
)
def open_help(clck):
    return True