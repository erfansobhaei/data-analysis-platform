apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y locales netcat-openbsd gcc gettext gcc && \
    apt-get clean

/usr/local/bin/python -m pip install --upgrade pip

pip install -r requirements.txt

gunicorn index:server  --bind 0.0.0.0:8000
