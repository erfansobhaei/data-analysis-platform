import os

import dash_bootstrap_components as dbc
import dash_uploader as du
import pandas as pd
from dash import dcc
from dash import html
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import declarative_base

from DB.DbContext import DbContext
from DB.Table import Base
from DB.example import example
from app import app
from apps import DBSCAN
from apps import KNN
from apps import PCA
from apps import agglomerative_clustering
from apps import bar_plot
from apps import decision
from apps import dendrogram
from apps import distribution_plot
from apps import dtable
from apps import edit
from apps import k_means_clustering
from apps import line_plot
from apps import missing
from apps import normalization
from apps import scatter_plot
from apps import summary
from apps import login
from apps import about
from apps import mismatch

app.title = "Data Analysis Platform"
FILEPATH = "./.var/uploads/"
du.configure_upload(app, FILEPATH)
df = pd.DataFrame()

SIDEBAR_STYLE = {
    "width": "80%",
    "margin": "auto",
    "padding": "1rem",
    "background-color": "#FAF8FF",
    "display": "flex",
    "justify-content": "center",
    "align-items": "center",
    "flex-direction": "column",
}

# padding for the page content
CONTENT_STYLE = {
    "background-color": "#FAF8FF",
}

HIDE = {
    "display": "none",
}

upload_style = {
    "width": "85%",
    "height": "70px",
    "lineHeight": "15px",
    "borderWidth": "1px",
    "borderStyle": "dashed",
    "borderRadius": "5px",
    "textAlign": "center",
    "margin": "auto",
    "position": "absolute",
    "bottom": 10,
    "display": "flex",
    "justify-content": "center",
    "align-items": "center",
}

upload_style_remove = {
    "width": "85%",
    "height": "auto",
    "lineHeight": "10px",
    "borderWidth": "1px",
    "borderRadius": "5px",
    "border": "dashed",
    "textAlign": "center",
    "margin": "auto",
    "position": "absolute",
    "bottom": 10,
    "display": "flex",
    "justify-content": "center",
    "align-items": "center",
    "padding-top": "1rem",
}

upload_div = html.Div(["Drag and Drop or ", html.A("Select Files")], id="div-upload")

upload_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/icons8-upload-96.png", width="50px", style={"margin-right": "10px"}
            ),
            du.Upload(
                id="dash_uploader",
                cancel_button=True,
                text="Click here for select file Or drag your file and drop here"
                # max_file_size=20,  # 20 Mb
                # filetypes=['csv', 'zip']
            ),

        ], style={"display": "flex", "align-items": "center", "justify-content": "center"}
    ), style={"background-color": "white", "border-color": "#A6AFFF", "border-radius": "35px", "width": "50%"},
    className="upload-hov"
)

view_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/icons8-document-250.png", width="50%"
            ),
            dbc.NavLink(
                "View",
                active="exact",
                id="dtable_link",
                disabled=False,
            )
        ], style={"display": "flex", "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"background-color": "white", "border-color": "#A6AFFF", "border-radius": "35px"}
)

edit_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/icons8-edit-384.png", width="50%"
            ),
            dbc.NavLink(
                "Edit",
                active="exact",
                id="edit_link",
                disabled=False,
            )
        ], style={"display": "flex", "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"background-color": "white", "border-color": "#A6AFFF", "border-radius": "35px"}
)

summary_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/icons8-total-sales-96.png", width="50%"
            ),
            dbc.NavLink(
                "Summary",
                active="exact",
                id="summary_link",
                disabled=False,
            )
        ], style={"display": "flex", "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"background-color": "white", "border-color": "#A6AFFF", "border-radius": "35px"}
)

delete_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/icons8-remove-96.png", width="50%"
            ),
            dbc.NavLink(
                "Delete",
                style={"color": "#E74C3C", "text-decoration": "none"}
            )
        ], style={"display": "flex", "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"background-color": "white", "border-color": "#E74C3C", "border-radius": "35px"}
)

download_card = dbc.Card(
    dbc.CardBody(
        [
            dcc.Download(id="download_section"),
            html.Img(
                src="./assets/icons8-download-96.png", width="50%"
            ),
            dbc.NavLink(
                "Export Data",
                style={"color": "white", "text-decoration": "none"}
            )
        ], style={"border-color": "white", "border-radius": "35px", "background-color": "#5d6eff", "display": "flex",
                  "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"border-radius": "35px"}
)

upload_section = dbc.Row([
    html.Div(
        [
            html.H4("Upload", style={"color": "#000"}),
            dbc.Row(
                [
                    html.Div(upload_card, style={"width": "100%"}),
                ],
                style={"margin-top": "15px"}
            ),
            html.Hr(className="my-2"),
        ],
        className="p-4", style={"width": "100%"}
    ),
], style={"width": "100%"}, id="upload_section")

dataset_section = dbc.Row([
    html.Div(
        [
            html.H4("Dataset", style={"color": "#000"}),
            dbc.Row(
                [
                    html.A(view_card, id="click_view", style={"width": "175px", "text-decoration": "none", },
                           href="/apps/dtable"),
                    html.A(edit_card, id="click_edit", style={"width": "175px", "text-decoration": "none", },
                           href="/apps/edit"),
                    html.A(summary_card, id="click_summary", style={"width": "175px", "text-decoration": "none", },
                           href="/apps/summary"),
                    html.A(download_card, id="click_download", style={"width": "175px", "text-decoration": "none", },
                           href="#"),
                    html.A(delete_card, id="click_delete", style={"width": "175px", "text-decoration": "none", },
                           href="/delete"),
                ],
                style={"margin-top": "15px"}, id="dataset_section"
            ),
            html.Hr(className="my-2"),
        ],
        className="p-4", style={"width": "100%"}
    ),
], style={"width": "100%"})

line_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/icons8-graph-96.png", width="50%"
            ),
            dbc.NavLink(
                "Line Plot",
                active="exact",
                id="line_plot_link",
                disabled=True,
            ),
        ], style={"display": "flex", "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"background-color": "white", "border-color": "#A6AFFF", "border-radius": "35px"}
)

scatter_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/icons8-scatter-plot-96.png", width="50%"
            ),
            dbc.NavLink(
                "Scatter Plot",
                active="exact",
                id="scatter_plot_link",
                disabled=True,
            ),
        ], style={"display": "flex", "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"background-color": "white", "border-color": "#A6AFFF", "border-radius": "35px"}
)

bar_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/icons8-bar-chart-96.png", width="50%"
            ),
            dbc.NavLink(
                "Bar Plot",
                active="exact",
                id="distribution_plot_link",
                disabled=True,
            ),
        ], style={"display": "flex", "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"background-color": "white", "border-color": "#A6AFFF", "border-radius": "35px"}
)

dist_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/icons8-normal-distribution-histogram-96.png", width="40%"
            ),
            dbc.NavLink(
                "Distribution Plot",
                active="exact",
                id="bar_plot_link",
                disabled=True,
            ),
        ], style={"display": "flex", "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"background-color": "white", "border-color": "#A6AFFF", "border-radius": "35px", "max-height": "132px"}
)

plot_section = dbc.Row([
    html.Div(
        [
            html.H4("Plots", style={"color": "#000"}),
            dbc.Row(
                [
                    # html.Div(upload_card, style={"width":"100%"}),
                    html.A(line_card, id="click_line", style={"width": "175px", "text-decoration": "none", },
                           href="/apps/line_plot"),
                    html.A(scatter_card, id="click_scatter", style={"width": "175px", "text-decoration": "none", },
                           href="/apps/scatter_plot"),
                    html.A(bar_card, id="click_bar", style={"width": "175px", "text-decoration": "none", },
                           href="/apps/bar_plot"),
                    html.A(dist_card, id="click_dist", style={"width": "225px", "text-decoration": "none", },
                           href="/apps/distribution_plot"),
                ],
                style={"margin-top": "15px"}, id="plot_section"
            ),
            html.Hr(className="my-2"),
        ],
        className="p-4", style={"width": "100%"}
    ),
], style={"width": "100%"})

knn_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/knn.png", width="50%"
            ),
            dbc.NavLink(
                "KNN",
                active="exact",
                id="knn_link",
                disabled=True,
            ),
        ], style={"display": "flex", "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"background-color": "white", "border-color": "#A6AFFF", "border-radius": "35px", "height": "132px"}
)

decision_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/decision.png", width="40%"
            ),
            dbc.NavLink(
                "Decision Tree",
                active="exact",
                id="decision_link",
                disabled=True,
            ),
        ], style={"display": "flex", "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"background-color": "white", "border-color": "#A6AFFF", "border-radius": "35px", "height": "132px"}
)

classifier_section = dbc.Row([
    html.Div(
        [
            html.H4("Classification Algorithems", style={"color": "#000"}),
            dbc.Row(
                [
                    html.A(knn_card, id="click_knn", style={"width": "175px", "text-decoration": "none", },
                           href="/apps/knn"),
                    html.A(decision_card, id="click_decision", style={"width": "200px", "text-decoration": "none", },
                           href="/apps/decision_tree"),
                ],
                style={"margin-top": "15px"}, id="classifier_section"
            ),
            html.Hr(className="my-2"),
        ],
        className="p-4", style={"width": "100%"}
    ),
], style={"width": "100%"})

kmean_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/icons8-cluster-64.png", width="40%"
            ),
            dbc.NavLink(
                "K Means",
                active="exact",
                id="kmc_link",
                disabled=True,
            ),
        ], style={"display": "flex", "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"background-color": "white", "border-color": "#A6AFFF", "border-radius": "35px", "max-height": "132px"}
)

aggc_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/icons8-hierarchical-structure-64.png", width="40%"
            ),
            dbc.NavLink(
                "Agglomorative",
                active="exact",
                id="aggc_link",
                disabled=True,
            ),
        ], style={"display": "flex", "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"background-color": "white", "border-color": "#A6AFFF", "border-radius": "35px", "max-height": "132px"}
)

dbscan_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/icons8-grid-3-96.png", width="40%"
            ),
            dbc.NavLink(
                "DBSCAN",
                active="exact",
                id="dbscan_link",
                disabled=True,
            ),
        ], style={"display": "flex", "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"background-color": "white", "border-color": "#A6AFFF", "border-radius": "35px", "max-height": "132px"}
)

cluster_section = dbc.Row([
    html.Div(
        [
            html.H4("Clustering Algorithems", style={"color": "#000"}),
            dbc.Row(
                [
                    html.A(kmean_card, id="click_kmean", style={"width": "175px", "text-decoration": "none", },
                           href="/apps/k_means_clustering"),
                    html.A(aggc_card, id="click_aggc", style={"width": "175px", "text-decoration": "none", },
                           href="/apps/agg_clustering"),
                    html.A(dbscan_card, id="click_dbscan", style={"width": "175px", "text-decoration": "none", },
                           href="/apps/dbscan"),
                ],
                style={"margin-top": "15px"}, id="cluster_section"
            ),
            html.Hr(className="my-2"),
        ],
        className="p-4", style={"width": "100%"}
    ),
], style={"width": "100%"})

missing_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/icons8-not-available-96.png", width="40%"
            ),
            dbc.NavLink(
                "Missing Values",
                active="exact",
                id="miss_link",
                disabled=True,
            ),
        ], style={"display": "flex", "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"background-color": "white", "border-color": "#A6AFFF", "border-radius": "35px", "max-height": "132px"}
)

mismatch_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/mismatch.jpg", width="40%"
            ),
            dbc.NavLink(
                "Mismatch Values",
                active="exact",
                id="mismatch_link",
                disabled=True,
            ),
        ], style={"display": "flex", "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"background-color": "white", "border-color": "#A6AFFF", "border-radius": "35px", "max-height": "132px"}
)

norm_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/icons8-bell-curve-64.png", width="40%"
            ),
            dbc.NavLink(
                "Normalization",
                active="exact",
                id="norm_link",
                disabled=True,
            ),
        ], style={"display": "flex", "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"background-color": "white", "border-color": "#A6AFFF", "border-radius": "35px", "height": "132px"}
)

pca_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/icons8-abscissa-96.png", width="40%"
            ),
            dbc.NavLink(
                "PCA",
                active="exact",
                id="pca_link",
                disabled=True,
            ),
        ], style={"display": "flex", "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"background-color": "white", "border-color": "#A6AFFF", "border-radius": "35px", "height": "132px"}
)

preprocess_section = dbc.Row([
    html.Div(
        [
            html.H4("Preprocessing Data", style={"color": "#000"}),
            dbc.Row(
                [
                    html.A(missing_card, id="click_missing", style={"width": "200px", "text-decoration": "none", },
                           href="/apps/missing"),
                    html.A(mismatch_card, id="click_mismatch", style={"width": "225px", "text-decoration": "none", },
                           href="/apps/mismatch"),
                    html.A(norm_card, id="click_norm", style={"width": "175px", "text-decoration": "none", },
                           href="/apps/normalization"),
                    html.A(pca_card, id="click_pca", style={"width": "175px", "text-decoration": "none", },
                           href="/apps/pca"),
                    

                ],
                style={"margin-top": "15px"}, id="prep_section"
            ),
            html.Hr(className="my-2"),
        ],
        className="p-4", style={"width": "100%"}
    ),
], style={"width": "100%"})

about_card = dbc.Card(
    dbc.CardBody(
        [
            html.Img(
                src="./assets/icons8-group-64.png", width="50%"
            ),
            dbc.NavLink(
                "About Us",
                active="exact",
                id="about_link",
                disabled=True,
            ),
        ], style={"display": "flex", "align-items": "center", "justify-content": "center", "flex-direction": "column"}
    ), style={"background-color": "white", "border-color": "#A6AFFF", "border-radius": "35px", "height": "132px"}
)

about_section = dbc.Row([
    html.Div(
        [
            html.H4("Our Team", style={"color": "#000"}),
            dbc.Row(
                [
                    html.A(about_card, id="click_about", style={"width": "175px", "text-decoration": "none", },
                           href="/apps/about"),

                ],
                style={"margin-top": "15px"}, id="about_section"
            ),
            html.Hr(className="my-2"),
        ],
        className="p-4", style={"width": "100%"}
    ),
], style={"width": "100%"})

kharazmi = dbc.Row([
    html.Div(
        [
            dbc.Row(
                [
                    html.Img(src="./assets/khu.png", width="40%")
                ],
                style={"margin-top": "15px"}
            ),
        ],
        className="p-4", style={"width": "25%"}
    ),
], style={"width": "50%", "display": "flex", "align-items": "center", "justify-content": "center"})

main_layout = dbc.Container(
    [
        upload_section,
        dataset_section,
        preprocess_section,
        plot_section,
        classifier_section,
        cluster_section,
        about_section,
        kharazmi,
        html.Div(style=HIDE, id="file-name"),
    ],
    style=SIDEBAR_STYLE,
)

modal = html.Div(
    [
        dbc.Modal(
            [
                dbc.ModalHeader(html.H5("No Dataset Available!", style={"font-size": "25px"})),
                html.Br(),
                dbc.ModalBody("You have not uploaded your data yet! Please upload a dataset to proceed"),
                html.Br(),
            ],
            id="modal",
            is_open=False,
        ),
    ]
)

app.layout = html.Div(
    [
        dcc.Store(storage_type="session", id="store"),
        dcc.Location(id="url", refresh=False),
        html.Div([main_layout], id="page-content", style=CONTENT_STYLE),
        html.Div(id="dummy", style=HIDE),
        modal
    ]
)


@app.callback(
    Output("page-content", "children"),
    Output("upload_section", "style"),
    Output("url", "pathname"),
    Input("url", "pathname"),
    Input('dash_uploader', 'isCompleted'),
    State('dash_uploader', 'upload_id'),
    State("upload_section", "style"),
)
def display_page(pathname, is_comp, upload_id, style):
    if pathname == "/login":
        view = login.layout()
        return view, HIDE, pathname
    elif pathname == "/apps/about":
        view = about.layout()
        return view, HIDE, pathname
    isdir = os.path.isdir(FILEPATH + upload_id)
    if not isdir:
        view = main_layout
        return view, style, "/"
    else:
        files = os.listdir(FILEPATH + upload_id)
        if len(files) != 0:
            df = pd.read_csv(FILEPATH + upload_id + '/' + files[0])
            if pathname == "/apps/k_means_clustering":
                view = k_means_clustering.layout(df)
            elif pathname == "/apps/line_plot":
                view = line_plot.layout(df)
            elif pathname == "/apps/scatter_plot":
                view = scatter_plot.layout(df)
            elif pathname == "/apps/bar_plot":
                view = bar_plot.layout(df)
            elif pathname == "/apps/distribution_plot":
                view = distribution_plot.layout(df)
            elif pathname == "/apps/dtable":
                view = dtable.layout(df)
            elif pathname == "/apps/agg_clustering":
                view = agglomerative_clustering.layout(df)
            elif pathname == "/apps/dendrogram":
                view = dendrogram.layout(df)
            elif pathname == "/apps/summary":
                view = summary.layout(df)
            elif pathname == "/apps/edit":
                view = edit.layout(df, upload_id)
            elif pathname == "/apps/missing":
                view = missing.layout(df, upload_id)
            elif pathname == "/apps/normalization":
                view = normalization.layout(df, upload_id)
            elif pathname == "/apps/pca":
                view = PCA.layout(df, upload_id)
            elif pathname == "/apps/knn":
                view = KNN.layout(df, upload_id)
            elif pathname == "/apps/decision_tree":
                view = decision.layout(df, upload_id)
            elif pathname == "/apps/dbscan":
                view = DBSCAN.layout(df, upload_id)
            elif pathname == "/apps/mismatch":
                view = mismatch.layout(df, upload_id)
            elif pathname == "/delete":
                view = main_layout
                pathname = "/"
                # Get all files uploaded in current session
                if os.path.isdir(FILEPATH + upload_id):
                    files = os.listdir(FILEPATH + upload_id)
                    for filename in files:
                        # Delete all the files
                        os.remove(FILEPATH + upload_id + '/' + filename)
                    return view, style, pathname
            else:
                view = main_layout
                pathname = "/"
        else:
            view = main_layout
            return view, style, "/"
        return view, HIDE, pathname


@app.callback(
    Output("modal", "is_open"),
    Output("click_view", "href"),
    Output("click_edit", "href"),
    Output("click_summary", "href"),
    Output("click_delete", "href"),
    Output("click_line", "href"),
    Output("click_scatter", "href"),
    Output("click_bar", "href"),
    Output("click_dist", "href"),
    Output("click_kmean", "href"),
    Output("click_aggc", "href"),
    Output("click_missing", "href"),
    Output("click_norm", "href"),
    Output("click_pca", "href"),
    Output("click_knn", "href"),
    Output("click_decision", "href"),
    Output("click_download", "href"),
    Output("click_dbscan", "href"),
    Output("click_mismatch", "href"),
    Input("click_view", "n_clicks"),
    Input("click_edit", "n_clicks"),
    Input("click_summary", "n_clicks"),
    Input("click_delete", "n_clicks"),
    Input("click_line", "n_clicks"),
    Input("click_scatter", "n_clicks"),
    Input("click_bar", "n_clicks"),
    Input("click_dist", "n_clicks"),
    Input("click_kmean", "n_clicks"),
    Input("click_aggc", "n_clicks"),
    Input("click_missing", "n_clicks"),
    Input("click_norm", "n_clicks"),
    Input("click_pca", "n_clicks"),
    Input("click_knn", "n_clicks"),
    Input("click_decision", "n_clicks"),
    Input("click_download", "n_clicks"),
    Input("click_dbscan", "n_clicks"),
    Input("click_mismatch", "n_clicks"),
    State("click_view", "href"),
    State("click_edit", "href"),
    State("click_summary", "href"),
    State("click_delete", "href"),
    State("click_line", "href"),
    State("click_scatter", "href"),
    State("click_bar", "href"),
    State("click_dist", "href"),
    State("click_kmean", "href"),
    State("click_aggc", "href"),
    State("click_missing", "href"),
    State("click_norm", "href"),
    State("click_pca", "href"),
    State("click_knn", "href"),
    State("click_decision", "href"),
    State("click_download", "href"),
    State("click_dbscan", "href"),
    State("click_mismatch", "href"),
    State("modal", "is_open"),
    State('upload_section', 'style'),
    prevent_initial_call=True,
)
def no_upload_error(i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i16, i17, i18, h1, h2, h3, h4, h5, h6,
                    h7, h8, h9, h10,
                    h11, h12, h13, h14, h15, h16, h17, h18, is_open, style):
    is_clicked = False
    inputs = [i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i16, i17, i18]
    for i in inputs:
        if isinstance(i, int):
            is_clicked = True
    if style == HIDE:
        return is_open, h1, h2, h3, h4, h5, h6, h7, h8, h9, h10, h11, h12, h13, h14, h15, h16, h17, h18
    elif not is_clicked:
        return is_open, "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"
    else:
        return not is_open, "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"


@app.callback(
    Output("download_section", "data"),
    Input("click_download", "n_clicks"),
    State('dash_uploader', 'upload_id'),
    prevent_initial_call=True,
)
def download_data(n_clicks, uid):
    if not os.path.isdir(FILEPATH + uid):
        raise PreventUpdate
    elif len(os.listdir(FILEPATH + uid)) == 0:
        raise PreventUpdate
    filename = os.listdir(FILEPATH + uid)[0]
    download = pd.read_csv(FILEPATH + uid + '/' + filename)
    return dcc.send_data_frame(download.to_csv, filename, index=False, encoding="utf-8-sig")


def del_file(path):
    print("DELETE -->", path)
    files = os.listdir(path)
    for filename in files:
        os.remove(path + '/' + filename)


def getfile_from_url(url, has_date=False):
    if has_date:
        df = pd.read_csv(url, parse_dates=True, keep_date_col=True)
    else:
        df = pd.read_csv(url)
    return df


server = app.server

if __name__ == "__main__":
    db_context = DbContext()
    example(db_context)
    app.run_server(debug=True)
