
# Data analysis platform

This project is python-based online data analysis platform developed by the students of Kharazmi University


## How to use online
This platform is publically available under [this link](https://dig-government.com/)


## How to run locally

Clone the project

```bash
  git clone https://gitlab.com/erfansobhaei/data-analysis-platform.git
```

Go to the project directory

```bash
  cd <dir>
```

Install dependencies from <requirements.txt> file available in project

```bash
  pip install -r /path/to/requirements.txt
```

run <index.py> file

```bash
  py index.py
```
The platform will be runnig in port 8050 of your local computer. you can open this with any browser (preferably with chrome)
```bash
http://127.0.0.1:8050/
```
## License

[MIT](https://choosealicense.com/licenses/mit/)

